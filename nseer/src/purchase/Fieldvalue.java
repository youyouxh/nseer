/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package purchase;

import java.sql.*;
import include.nseer_db.nseer_db;

public class Fieldvalue {

    private String dbase = "";
    private String table = "";
    private String fieldName1 = "";
    private String fieldValue1 = "";
    private String fieldName2 = "";
    private String value = "";
    private String sql = "";
    private ResultSet rs = null;

    public String getValue(String dbase, String table, String field1, String value1, String field2) {
        this.dbase = dbase;
        this.table = table;
        this.fieldName1 = field1;
        this.fieldValue1 = value1;
        this.fieldName2 = field2;
        nseer_db dba = new nseer_db(dbase);
        try {
            sql = "select * from " + table + " where " + field1 + "='" + fieldValue1 + "'";
            rs = dba.executeQuery(sql);
            if (rs.next()) { 
                value = rs.getString(fieldName2);
            }
            dba.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return value;
    }
    
    public String getValue(String dbase, String table, String field1, String value1) {
        this.dbase = dbase;
        this.table = table;
        this.fieldName1 = field1;
        this.fieldValue1 = value1;
        Double demand_cost_price_sum=0.0d;
    	Double bonus_sum=0.0d;
        nseer_db dba = new nseer_db(dbase);
        try {
            sql = "select * from " + table + " where " + field1 + "='" + fieldValue1 + "' and gather_tag='3' and salary_tag='0'";
            rs = dba.executeQuery(sql);
            if (rs.next()) { 
            	demand_cost_price_sum+=rs.getDouble("sale_price_sum");
            	if(rs.getString("bonus_calculate_type").equals("按销售额计算")){
            	bonus_sum+=rs.getDouble("order_sale_bonus_sum");
            	}else{
            	bonus_sum+=rs.getDouble("order_profit_bonus_sum");
            	}
            }
            dba.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if(bonus_sum==0)return "0";
        else return bonus_sum.toString();
    }
    public String getValue(String dbase, String table, String field1, String value1, String field2,String value2,String field3,String value3,String field4) {
        this.dbase = dbase;
        this.table = table;
        this.fieldName1 = field1;
        this.fieldValue1 = value1;
        value="";
        nseer_db dba = new nseer_db(dbase);
        try {
            sql = "select * from " + table + " where " + field1 + "='" + fieldValue1 + "' and "+field2+"='"+value2+"' and "+field3+"='"+value3+"'";
            rs = dba.executeQuery(sql);
            if (rs.next()) {
                value = rs.getString(field4);
            }
            dba.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if(value.equals("")) return "0";
        return value;
    }
}