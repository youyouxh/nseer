/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package oa.intrmessage;
 
 
import include.nseer_cookie.Email;
import include.nseer_cookie.Note;
import include.nseer_cookie.exchange;
import include.nseer_db.nseer_db_backup1;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.PageContext;

public class check_type_ok extends HttpServlet{

public synchronized void service(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException{
HttpSession dbSession=request.getSession();
JspFactory _jspxFactory=JspFactory.getDefaultFactory();
PageContext pageContext = _jspxFactory.getPageContext(this,request,response,"",true,8192,true);
ServletContext dbApplication=dbSession.getServletContext();
int finished_tag=0;
try{

ServletContext application;
HttpSession session=request.getSession();
ServletContext context=session.getServletContext();
String path=context.getRealPath("/");
nseer_db_backup1 oa_db=new nseer_db_backup1(dbApplication);
if(oa_db.conn((String)dbSession.getAttribute("unit_db_name"))){

Note note=new Note();
Email mail=new Email();

String intrmessage_ID=request.getParameter("intrmessage_ID");
String table=request.getParameter("table");
String table_id=request.getParameter("table_id");
String table_name=request.getParameter("table_name");
String checker=request.getParameter("checker");
String checker_ID=request.getParameter("checker_ID");
String check_time=request.getParameter("check_time");
String[] check_type=request.getParameterValues("check_type");
String[] chain_id=request.getParameterValues("chain_id");
String messager_type="";
if(table.equals("crm_file")){
	messager_type="客户";
}else if(table.equals("purchase_file")){
	messager_type="采购供应商";
}else if(table.equals("intrmanufacture_file")){
	messager_type="委外厂商";
}else if(table.equals("logistics_file")){
	messager_type="配送单位";
}
String send_type="";
if(check_type!=null){
	for(int i=0;i<check_type.length;i++){
		send_type+=check_type[i]+",";
	}
	send_type=send_type.substring(0,send_type.length()-1);
}
int n=0;
int check_amount=0;
String content="";
String file_name="";
String subject="";
String sql2="select * from oa_intrmessage where intrmessage_ID='"+intrmessage_ID+"'";
ResultSet rs2=oa_db.executeQuery(sql2);
if(rs2.next()){
	check_amount=rs2.getInt("check_amount")+1;
	subject=exchange.toHtml(rs2.getString("subject"));
	content=rs2.getString("content");
	file_name=rs2.getString("attachment1");
	File file=new File(path+"oa/file_attachments/"+file_name);

if(chain_id==null||check_type==null){
	
  	response.sendRedirect("oa/intrmessage/check_ok.jsp?finished_tag=0");
}else{
	String[] email_box=new String[chain_id.length];
	n=0;
int p=0;
for(int j=0;j<chain_id.length;j++){
	//String checkbox_name="col"+j;
	//String[] cols=request.getParameterValues(checkbox_name);
	
	
		if(!chain_id[j].equals("")){
			n++;
				StringTokenizer token=new StringTokenizer(chain_id[j],"/");
					while(token.hasMoreTokens()){
						String ID=token.nextToken();
						String name=token.nextToken();
						String sql3="select * from "+table+" where "+table_id+"='"+ID+"'";
						ResultSet rs3=oa_db.executeQuery(sql3);
						if(rs3.next()){
							email_box[p]=rs3.getString("contact_person1_email");
							p++;
						String sql="insert into oa_intrmessage_details(intrmessage_ID,chain_ID,chain_name,type,messager_ID,messager_name,batch_amount,cell,email,messager_type) values('"+intrmessage_ID+"','"+rs3.getString("chain_ID")+"','"+rs3.getString("chain_name")+"','"+send_type+"','"+ID+"','"+name+"','"+check_amount+"','"+rs3.getString("contact_person1_mobile")+"','"+rs3.getString("contact_person1_email")+"','"+messager_type+"')";
						oa_db.executeUpdate(sql);
						if(send_type.indexOf("发短信")!=-1){
						note.send("bjnseer","8888",rs3.getString("contact_person1_mobile"),content);
						}
						}
					}
		}
	
	
}
if(send_type.indexOf("发邮件")!=-1){
	mail.send(email_box,"smtp.sina.com.cn","yhuser@sina.com","123456",subject,content);
}
if(n==0){
  	response.sendRedirect("oa/intrmessage/check_ok.jsp?finished_tag=1");
	}else{
	String sql1="update oa_intrmessage set checker='"+checker+"',checker_ID='"+checker_ID+"',check_time='"+check_time+"',check_type='"+send_type+"',check_amount='"+check_amount+"',check_tag='1' where intrmessage_ID='"+intrmessage_ID+"'";
	oa_db.executeUpdate(sql1);
response.sendRedirect("oa/intrmessage/check_ok.jsp?finished_tag=2");
	}
}
}else{

  	response.sendRedirect("oa/intrmessage/check_ok.jsp?finishedd_tag=3");
}
oa_db.commit();
oa_db.close();
}else{
	response.sendRedirect("error_conn.htm");
}
}
catch (Exception ex){
ex.printStackTrace();
}
}
}