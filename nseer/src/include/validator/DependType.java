/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.validator;
public interface DependType {
    String INTERGER = "integer";
    String DOUBLE = "double";
    String DATE = "date";
    String EMAIL = "email";
    String REQUIRED = "required";
}
