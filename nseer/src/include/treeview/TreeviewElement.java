package include.treeview;

public interface TreeviewElement {

    public String getNodeName();

    public String getNodeUrl();

    public boolean canExpand();

    public TreeviewElement[] getChildren();

    public int getID();
}
