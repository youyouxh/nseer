/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.nseer_cookie;

import java.util.ArrayList;

public class arraySynthesis {

    public String[] getArraySynthesis(String[] a, String[] b) {
        ArrayList c = new ArrayList();
        for (int i = 0; i < a.length; i++) {
            c.add(c.size(), a[i]);
        }
        for (int i = 0; i < b.length; i++) {
            c.add(c.size(), b[i]);
        }
        ArrayList d = new ArrayList();
        for (int i = 0; i < c.size(); i++) {
            if (!d.contains(c.get(i))) {
                d.add(c.get(i));
            }
        }
        String[] f = new String[d.size()];
        for (int i = 0; i < d.size(); i++) {
            f[i] = (String) d.get(i);
        }
        return f;
    }

    public static void main(String[] args) {
        String[] a = {"a", "b", "c", "d", "e"};
        String[] b = {"a", "b", "c", "d", "f", "g", "r", "k", "a"};
        arraySynthesis demo = new arraySynthesis();
        for (int i = 0; i < demo.getArraySynthesis(a, b).length; i++) {
        }
    }
}
