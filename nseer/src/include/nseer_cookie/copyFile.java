/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.nseer_cookie;

import java.io.*;

public class copyFile {

    String sourcePath;
    String destinationPath;
    int buffer;
    long fileLength;
    FileInputStream sourceFile;
    FileOutputStream destinationFile;
    boolean makeDirs;
    boolean isMoving;
    boolean fieldSss;

    public copyFile(String sourcePath, String destinationPath) {
        this.sourcePath = sourcePath;
        this.destinationPath = destinationPath;
        buffer = 2048;
        fileLength = 0L;
        sourceFile = null;
        destinationFile = null;
        makeDirs = false;
        isMoving = false;
        fieldSss = false;
    }

    public int copy() {
        File sourceFile = new File(sourcePath);
        File destFile = new File(destinationPath);
        File destinationFile = null;
        FileInputStream sourceFileInputStream = null;
        FileOutputStream destinationFileOutputStream = null;
        byte buf[] = new byte[buffer];
        int counter = 0;
        if (!sourceFile.exists()) {
            return -1;
        }
        if (sourceFile.isDirectory()) {
            return -2;
        }
        try {
            if (destFile.isDirectory()) {
                String srcName = sourceFile.getName();
                destinationPath += srcName;
            }
            destinationFile = new File(destinationPath);
            String parentDirectory = destinationFile.getParent();
            File parentDirFile = new File(parentDirectory);
            if (!parentDirFile.exists()) {
                if (!makeDirs) {
                    return -3;
                }
                if (makeDirs) {
                    parentDirFile.mkdirs();
                }
            }
        } catch (NullPointerException _ex) {
        }
        long oldFileLength = fileLength;
        fileLength = sourceFile.length();
        try {
            sourceFileInputStream = new FileInputStream(sourceFile);
            destinationFileOutputStream = new FileOutputStream(destinationFile);
            while ((counter = sourceFileInputStream.read(buf)) != -1) {
                destinationFileOutputStream.write(buf, 0, counter);
            }
        } catch (IOException e) {
            return -4;
        }
        if (destinationFileOutputStream != null) {
            try {
                destinationFileOutputStream.close();
            } catch (IOException e) {
                return -4;
            }
        }
        try {
            sourceFileInputStream.close();
        } catch (IOException e) {
            return -4;
        }
        return 1;
    }

    public void setMakeDirs(boolean makeDirs) {
        boolean oldValue = this.makeDirs;
        this.makeDirs = makeDirs;
    }

    public static void main(String[] args) {
        copyFile cp = new copyFile("d:/cnbbs/face/face0.gif", "d:/gggggg/crm.gif");
        cp.setMakeDirs(false);
        cp.copy();
    }
}