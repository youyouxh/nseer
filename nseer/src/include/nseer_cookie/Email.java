/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.nseer_cookie;

import java.util.Date;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Email {

    public void send(String[] emailbox, String smtp, String from, String passwd, String subject, String content) {
        try {
            Properties props = new Properties();
			String[] sh=SmtpHost.getContent("ondemand1");
            props.put("mail.smtp.host", sh[0]);
            props.put("mail.smtp.auth", "true");
            Session s = Session.getDefaultInstance(props, null);
            s.setDebug(true);
            MimeMessage msg = new MimeMessage(s);
            InternetAddress fromAddress = new InternetAddress(sh[1]);
            msg.setFrom(fromAddress);
            for (int i = 0; i < emailbox.length; i++) {
                InternetAddress toAddress = new InternetAddress(emailbox[i]);
                if(emailbox.length==1){
                	msg.addRecipient(Message.RecipientType.TO, toAddress);
                }else{
                      msg.addRecipient(Message.RecipientType.BCC, toAddress);
                }
            }
            msg.setSubject(subject);
            BodyPart bp = new MimeBodyPart();
            bp.setContent(content, "text/html;charset=UTF-8");
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(bp);
            msg.setContent(mp);
            msg.setSentDate(new Date());
            msg.saveChanges();
            Transport transport = s.getTransport("smtp");
            transport.connect(sh[0], sh[1], sh[2]);
            transport.sendMessage(msg, msg.getAllRecipients());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}