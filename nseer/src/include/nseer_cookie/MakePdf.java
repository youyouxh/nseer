/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.nseer_cookie;

import java.io.*;
import java.sql.*;
import java.util.*;
import include.nseer_db.*;
import com.lowagie.text.*;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.*;
import javax.servlet.*;
import javax.servlet.http.*;
import include.excel_export.Masking;

public class MakePdf {

    private int fileAmount = 0;
    private int allpage = 0;
    private String configFile;

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public void make(String database, String tablename, String sql1, String sql2, String filename, int everypage, HttpSession session) {
        try {
            nseer_db aaa = new nseer_db(database);
            nseer_db demo_db = new nseer_db(database);
            ServletContext context = session.getServletContext();
            String path = context.getRealPath("/");
            Masking reader = new Masking(configFile);
            Vector columnNames = new Vector();
            Vector tables = reader.getTableNicks();
            Iterator loop = tables.iterator();
            while (loop.hasNext()) {
                String tablenick = (String) loop.next();
                columnNames = reader.getColumnNames(tablenick);
            }
            int cpage = 1;
            int spage = 1;
            int ipage = everypage;
            String pagesql = sql1;
            ResultSet pagers = demo_db.executeQuery(pagesql);
            pagers.next();
            int allCol = pagers.getInt("A");
            allpage = (int) Math.ceil((allCol + ipage - 1) / ipage);
            for (int m = 1; m <= allpage; m++) {
                spage = (m - 1) * ipage;
                String sql = sql2 + " limit " + spage + "," + ipage;
                ResultSet bbb = aaa.executeQuery(sql);
                int b = columnNames.size();
                int a = 0;
                while (bbb.next()) {
                    a++;
                }
                bbb.first();
                Rectangle rectPageSize = new Rectangle(PageSize.A4);
                rectPageSize = rectPageSize.rotate();
                Document document = new Document(rectPageSize, 20, 20, 20, 20);
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path + filename + m + ".pdf"));
                document.open();
                BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
                com.lowagie.text.Font FontChinese = new com.lowagie.text.Font(bfChinese, 8, com.lowagie.text.Font.NORMAL);
                Paragraph title1 = new Paragraph("nseer ERP", FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLDITALIC));
                Chapter chapter1 = new Chapter(title1, 1);
                chapter1.setNumberDepth(0);
                Paragraph title11 = new Paragraph(tablename, FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD));
                Section section1 = chapter1.addSection(title11);
                Table t = new Table(b, a);
                t.setPadding(1);
                t.setSpacing(0);
                t.setBorderWidth(1);
                do {
                    for (int k = 0; k < b; k++) {
                        Cell cell = new Cell(new Paragraph(bbb.getString((String) columnNames.elementAt(k)), FontChinese));
                        t.addCell(cell);
                    }
                } while (bbb.next());
                section1.add(t);
                document.add(chapter1);
                document.close();
            }
        } catch (Exception pp) {
            pp.printStackTrace();
        }
    }

    public int fileAmount() {
        fileAmount = allpage;
        return fileAmount;
    }
}