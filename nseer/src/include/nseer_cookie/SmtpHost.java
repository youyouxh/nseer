package include.nseer_cookie;

import java.sql.*;

import include.nseer_db.nseer_db;
import include.tree_index.Nseer;

import javax.servlet.http.HttpSession;

public class SmtpHost {
	
	public static String[] getContent(String db_name){
		String[] result=new String[3];
		Nseer n=new Nseer();
		try{
			nseer_db db=new nseer_db(db_name);
			String sql9="select * from erpplatform_config_public_char where kind='mailhost'";
			ResultSet rs9 = db.executeQuery(sql9);
			if(rs9.next()){
				result[0]=rs9.getString("type_name");
				result[1]=rs9.getString("describe1");
				result[2]=n.D(rs9.getString("describe2"));
			}else{
				result[0]="";
				result[1]="";
				result[2]="";
			}
			db.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
}