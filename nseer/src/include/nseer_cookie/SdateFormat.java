/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.nseer_cookie;

import include.nseer_db.nseer_db;
import java.sql.*;

public class SdateFormat {

	public static String format(String obj,String twins){
		String	result="";
		try{
			if(obj.equals("1800-01-01 00:00:00.0")){
				result=twins;
			}else{
				result=obj;
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	
	public static String formatId(String id){
		String	result="";
		try{
			if(id.indexOf("@")!=-1){
				result=id.split("@")[1];
			}else{
				result=id;
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	
	public static String formatS(String id,String s){
		String	result="";
		try{
			if(id.indexOf(s)!=-1){
				result=id.split(s)[1];
			}else{
				result=id;
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
}