/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.auto_execute;

import java.lang.reflect.Method;
import java.lang.reflect.*;
import java.util.*;

public class AutoExecute extends TimerTask {

    private ConfigReader reader = null;

    public AutoExecute() {
        reader = new ConfigReader();
    }

    public void run() {
        String[] classNames = reader.getClassNames();
        for (int i = 0; i < classNames.length; i++) {
            try {
                Class mclass = Class.forName(classNames[i]);
                Object obj = mclass.newInstance();
                String methodName = reader.getMethod(classNames[i]);
                Method method = mclass.getMethod(methodName, new Class[0]);
                method.invoke(obj, null);
            } catch (ClassNotFoundException cnfe) {
                cnfe.printStackTrace();
            } catch (InstantiationException ie) {
                ie.printStackTrace();
            } catch (NoSuchMethodException nsme) {
                nsme.printStackTrace();
            } catch (IllegalAccessException ile) {
                ile.printStackTrace();
            } catch (InvocationTargetException ive) {
                ive.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new AutoExecute().run();
    }
} 
