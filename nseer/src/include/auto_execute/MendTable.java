package include.auto_execute;

import include.nseer_db.nseer_db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MendTable {
     public void Mend(String path,nseer_db db) throws IOException{
    	 try {
    		String table_name1="";
    		File file=new File(path+"dbupdate.txt");
    		if(file.exists()){
    		   String one_inline="";
			   BufferedReader in=new  BufferedReader(new FileReader(file));
			   try {
				while((one_inline=in.readLine())!=null){
					if(!one_inline.equals("")){
					   String table_name2=one_inline.split(" ")[2];
					   if(!table_name2.equals(table_name1)){
						   table_name1=table_name2;
						   System.out.println("表"+table_name1+"升级完成！");
					   }
					   String sql=one_inline;
					   db.executeUpdate(sql);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			 db.close();
			 in.close();
			 file.delete();
    		}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
     }
}
