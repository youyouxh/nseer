/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.auto_execute;

import java.io.*;
import java.util.*;
import org.jdom.*;
import org.jdom.input.SAXBuilder;

public class ConfigReader extends Path {

    private String file = "";
    private SAXBuilder builder = null;
    private Document doc = null;
    private Element root = null;
    private int length = 0;
    private String[] classNames = null;
    private String[] methods = null;

    public ConfigReader() {
        file = getPath() + "/conf/auto.xml";
        try {
            builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser");
            doc = builder.build(new File(file));
            root = doc.getRootElement();
        } catch (Exception je) {
            je.printStackTrace();
        }
        setLength();
    }

    private void setLength() {
        int length = 0;
        List tasks = root.getChildren("task");
        Iterator it = tasks.iterator();
        while (it.hasNext()) {
            length++;
            it.next();
        }
        this.length = length;
    }

    private int getLength() {
        return length;
    }

    public String[] getClassNames() {
        if (this.classNames != null) {
            return this.classNames;
        }
        String[] result = new String[length];
        List tasks = root.getChildren("task");
        Iterator it = tasks.iterator();
        int index = 0;
        while (it.hasNext()) {
            Element task = (Element) it.next();
            result[index] = task.getChild("classname").getText();
            index++;
        }
        this.classNames = result;
        return result;
    }

    public String[] getMethods() {
        if (this.methods != null) {
            return this.methods;
        }
        String[] result = new String[length];
        List tasks = root.getChildren("task");
        Iterator it = tasks.iterator();
        int index = 0;
        while (it.hasNext()) {
            Element task = (Element) it.next();
            result[index] = task.getChild("method").getText();
            index++;
        }
        this.methods = result;
        return result;
    }

    public String getMethod(String className) {
        if (this.classNames == null ||
                this.methods == null) {
            this.classNames = getClassNames();
            this.methods = getMethods();
        }
        int index = indexOf(classNames, className);
        if (index == -1) {
            return null;
        } else if (index >= methods.length) {
            return null;
        }
        return methods[index];
    }

    private int indexOf(String[] array, String element) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        String[] names = new ConfigReader().getClassNames();
        if (names != null) {
            for (int i = 0; i < names.length; i++) {
            }
        }
    }
}
