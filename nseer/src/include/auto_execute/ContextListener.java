/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.auto_execute;

import include.multilanguage.InitLang;
import include.nseer_cookie.CreateJFile;
import include.nseer_cookie.DeleteJFile;
import include.nseer_cookie.KindDeep;
import include.nseer_cookie.delFile;
import include.nseer_cookie.getSome;
import include.nseer_db.connPool;
import include.nseer_db.nseer_db;

import java.io.File;
import java.util.Calendar;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public final class ContextListener implements ServletContextListener {

    private ServletContext context = null;
    private java.util.Timer timer = null;
    private static final int C_SCHEDULE_HOUR = 0;
    private static boolean isRunning = false;

    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        try {
            initDB initDB = new initDB();
            context.setAttribute("initDB", initDB);
            initDB.initDB(context);
            System.out.println("sync ok");
            System.out.println("正在创建数据库连接池，这个过程最长需要几分钟，请等待...");
            connPool connPool = new connPool("ondemand1", 0, context, 50);
            context.setAttribute("connPool", connPool);
            connPool connPool1 = new connPool("mysql", 0, context, 20);
            context.setAttribute("connPool1", connPool1);
            System.out.println("Pool ok");
            //设置精度
            context.setAttribute("nseerPrecision", KindDeep.getPre("ondemand1"));
            System.out.println("KindDeep.getPre done...");
            context.setAttribute("nseerAmountPrecision", KindDeep.getAmountPre("ondemand1"));
            System.out.println("KindDeep.getAmountPre done...");
            context.setAttribute("nseerDraft", "1");
            String path = context.getRealPath("/");
            //广告(从数据库取出js内容保存为nseergrid.js)
            CreateJFile c = new CreateJFile();
            c.create(path, "ondemand1");
            //创建目录结构
            delFile df = new delFile();
            df.delete(path + "xml/listPage/");
            File f = new File(path + "xml/listPage/");
            f.mkdir();
            //设置语言
            initML initML = new initML();
            System.out.println("initML done...");
            context.setAttribute("initML", initML);
            initML.initML(context);
            //设置项目路径
            resetDB resetDB = new resetDB();
            context.setAttribute("resetDB", resetDB);
            resetDB.resetDB(context);
            System.out.println("reset ok1");
            //设置过期时间
            ContactExpiry ContactExpiry = new ContactExpiry();
            context.setAttribute("ContactExpiry", ContactExpiry);
            ContactExpiry.flow(context);
            GatherSumLimit GatherSumLimit = new GatherSumLimit();
            context.setAttribute("GatherSumLimit", GatherSumLimit);
            GatherSumLimit.cost(context);
            GatherExpiry GatherExpiry = new GatherExpiry();
            context.setAttribute("GatherExpiry", GatherExpiry);
            GatherExpiry.back(context);
            PriceLimit PriceLimit = new PriceLimit();
            context.setAttribute("PriceLimit", PriceLimit);
            PriceLimit.price(context);
            InitLang initlang = new InitLang();
            initlang.init("ondemand1", context);
            System.out.println("Init language ok!");
            System.out.println("reset ok2");
            getSome getSome = new getSome();
            getSome.getSome();//空方法
            nseer_db db=new nseer_db("ondemand1");
            new MendTable().Mend(path, db);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        timer = new java.util.Timer(true);
        context.log("定时器启动");
        timer.schedule(new MyTask(), 0, 1500 * 1000);
        context.log("添加任务调度表");
    }

    public void contextDestroyed(ServletContextEvent event) {
        try {
            String path = context.getRealPath("/");
            DeleteJFile d = new DeleteJFile();
            d.delete(path);
        } catch (Exception ex) {
        }
        context = event.getServletContext();
        resetDB resetDB = (resetDB) context.getAttribute("resetDB");
        context.removeAttribute("resetDB");
        ContactExpiry ContactExpiry = (ContactExpiry) context.getAttribute("ContactExpiry");
        context.removeAttribute("ContactExpiry");
        GatherSumLimit GatherSumLimit = (GatherSumLimit) context.getAttribute("GatherSumLimit");
        context.removeAttribute("GatherSumLimit");
        GatherExpiry GatherExpiry = (GatherExpiry) context.getAttribute("GatherExpiry");
        context.removeAttribute("GatherExpiry");
        PriceLimit PriceLimit = (PriceLimit) context.getAttribute("PriceLimit");
        context.removeAttribute("PriceLimit");
        timer.cancel();
        context.log("定时器销毁");
    }

    class MyTask extends TimerTask {

        public void run() {
            Calendar cal = Calendar.getInstance();
            if (cal.get(Calendar.HOUR_OF_DAY) == 10) {
                context.log("开始执行指定任务");
                try {
                    resetDB resetDB = new resetDB();
                    context.setAttribute("resetDB", resetDB);
                    resetDB.resetDB(context);
                    ContactExpiry ContactExpiry = new ContactExpiry();
                    context.setAttribute("ContactExpiry", ContactExpiry);
                    ContactExpiry.flow(context);
                    GatherSumLimit GatherSumLimit = new GatherSumLimit();
                    context.setAttribute("GatherSumLimit", GatherSumLimit);
                    GatherSumLimit.cost(context);
                    GatherExpiry GatherExpiry = new GatherExpiry();
                    context.setAttribute("GatherExpiry", GatherExpiry);
                    GatherExpiry.back(context);
                    PriceLimit PriceLimit = new PriceLimit();
                    context.setAttribute("PriceLimit", PriceLimit);
                    PriceLimit.price(context);
                    System.out.println("reset ok3");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                context.log("指定任务执行结束");
            }
        }
    }
}
