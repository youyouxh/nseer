/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.excel_import;

public class Path {

    public Path() {
    }

    public String getPath() {
        String path = this.getClass().getClassLoader().getResource("/").getPath();
        return path;
    }

    public static void main(String[] args) {
        Path path = new Path();
    }
}
