/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.calculator;

import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;

public class SimpleCalculator implements Calculator {

    String[] _toks;

    public SimpleCalculator(String expression) {
        ArrayList list = new ArrayList();
        StringTokenizer tokenizer = new StringTokenizer(expression);
        while (tokenizer.hasMoreTokens()) {
            list.add(tokenizer.nextToken());
        }
        _toks = (String[]) list.toArray(new String[list.size()]);
    }

    public double evaluate(double[] args) {
        Stack stack = new Stack();
        for (int i = 0; i < _toks.length; i++) {
            String tok = _toks[i];
            if (tok.startsWith("$")) {
                int varnum = Integer.parseInt(tok.substring(1));
                stack.push(new Double(args[varnum]));
            } else {
                char opchar = tok.charAt(0);
                int op = "+-*/".indexOf(opchar);
                if (op == -1) {
                    stack.push(Double.valueOf(tok));
                } else {
                    double arg2 = ((Double) stack.pop()).doubleValue();
                    double arg1 = ((Double) stack.pop()).doubleValue();
                    switch (op) {
                        case 0:
                            stack.push(new Double(arg1 + arg2));
                            break;
                        case 1:
                            stack.push(new Double(arg1 - arg2));
                            break;
                        case 2:
                            stack.push(new Double(arg1 * arg2));
                            break;
                        case 3:
                            stack.push(new Double(arg1 / arg2));
                            break;
                        default:
                            throw new RuntimeException("操作符不合法: " + tok);
                    }
                }
            }
        }
        return ((Double) stack.pop()).doubleValue();
    }

    public double evaluate() {
        Stack stack = new Stack();
        for (int i = 0; i < _toks.length; i++) {
            String tok = _toks[i];
            if (tok.startsWith("$")) {
            } else {
                char opchar = tok.charAt(0);
                int op = "+-*/".indexOf(opchar);
                if (op == -1) {
                    stack.push(Double.valueOf(tok));
                } else {
                    double arg2 = ((Double) stack.pop()).doubleValue();
                    double arg1 = ((Double) stack.pop()).doubleValue();
                    switch (op) {
                        case 0:
                            stack.push(new Double(arg1 + arg2));
                            break;
                        case 1:
                            stack.push(new Double(arg1 - arg2));
                            break;
                        case 2:
                            stack.push(new Double(arg1 * arg2));
                            break;
                        case 3:
                            stack.push(new Double(arg1 / arg2));
                            break;
                        default:
                            throw new RuntimeException("操作符不合法: " + tok);
                    }
                }
            }
        }
        return ((Double) stack.pop()).doubleValue();
    }

    public static void main(String[] args) {
        double[] ag = new double[]{3, 6};
    }
}
