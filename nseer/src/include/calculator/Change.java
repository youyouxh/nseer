/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package include.calculator;

public class Change {

    private String word = null;

    public Change(String word) {
        this.word = word;
    }

    public String execute(String[] str1,
            String[] str2) {
        if (str1.length == str2.length) {
            for (int i = 0; i < str1.length; i++) {
                replace(str1[i], str2[i]);
            }
        }
        return word;
    }

    public void replace(String str1,
            String str2) {
        int index = 0;
        String temp = "";
        if ((index = word.indexOf(str1)) != -1) {
            if (index > 0) {
                temp += word.substring(0, index) + str2;
                word = word.substring(index + str1.length(), word.length());
                word = temp + word;
            } else if (index == 0) {
                temp += str2;
                word = word.substring(str1.length(), word.length());
                word = temp + word;
            }
            replace(str1, str2);
        } else {
            return;
        }
    }

    public static void main(String[] args) {
        String one = " 10 ";
        String[] str1 = new String[]{"three", "one", "five", "two"};
        String[] str2 = new String[]{" 3 ", one, " 5 ", " 2 "};
    }
}
