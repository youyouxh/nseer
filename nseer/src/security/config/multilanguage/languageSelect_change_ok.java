/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package security.config.multilanguage;

import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.*;
import java.sql.*;
import java.io.*;
import include.nseer_db.*;

public class languageSelect_change_ok extends HttpServlet {

    ServletContext application;
    HttpSession session;

    public synchronized void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession dbSession = request.getSession();
        JspFactory _jspxFactory = JspFactory.getDefaultFactory();
        PageContext pageContext = _jspxFactory.getPageContext(this, request, response, "", true, 8192, true);
        ServletContext dbApplication = dbSession.getServletContext();
        try {
            HttpSession session = request.getSession();
            PrintWriter out = response.getWriter();
            nseer_db_backup1 hr_db = new nseer_db_backup1(dbApplication);
            if (hr_db.conn((String) dbSession.getAttribute("unit_db_name"))) {
                String human_ID = (String) dbSession.getAttribute("human_IDD");
                String type_name = request.getParameter("type_name");
                String sqll = "select * from document_config_public_char where kind='语言类型'";
                ResultSet rs = hr_db.executeQuery(sqll);
                if (rs.next()) {
                    String sql2 = "update security_users set language='" + type_name + "' where human_ID='" + human_ID + "'";
                    hr_db.executeUpdate(sql2);
                    session.setAttribute("language", type_name);
                }
                hr_db.commit();
                hr_db.close();
                response.sendRedirect("security/config/multilanguage/languageSelect_change_ok_a.jsp");
            } else {
                response.sendRedirect("error_conn.htm");
            }
        } catch (Exception ex) {
        }
    }
}
