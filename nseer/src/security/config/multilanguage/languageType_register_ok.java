/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package security.config.multilanguage;

import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.*;
import java.sql.*;
import java.util.*;
import java.io.*;
import include.nseer_db.*;
import validata.ValidataNumber;

public class languageType_register_ok extends HttpServlet {

    ServletContext application;
    HttpSession session;
    nseer_db_backup1 erp_db = null;
    ValidataNumber validata = new ValidataNumber();

    public synchronized void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession dbSession = request.getSession();
        JspFactory _jspxFactory = JspFactory.getDefaultFactory();
        PageContext pageContext = _jspxFactory.getPageContext(this, request, response, "", true, 8192, true);
        ServletContext dbApplication = dbSession.getServletContext();
        application = dbSession.getServletContext();
        Hashtable tt = null;
        String key = "";
        try {
            PrintWriter out = response.getWriter();
            nseer_db_backup1 security_db = new nseer_db_backup1(dbApplication);
            if (security_db.conn((String) dbSession.getAttribute("unit_db_name"))) {
                String type_name = request.getParameter("type_name");
                int n = 0;
                String ok = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                for (int i = 0; i < type_name.length(); i++) {
                    if (ok.indexOf(type_name.substring(i, i + 1)) == -1) {
                        n++;
                        break;
                    }
                }
                if (!validata.validata(type_name) && n == 0) {
                    String sqll = "select * from document_config_public_char where kind='语言类型' and type_name='" + type_name + "'";
                    ResultSet rs = security_db.executeQuery(sqll);
                    if (rs.next()) {
                        response.sendRedirect("security/config/multilanguage/languageType_register_ok_a.jsp");
                    } else {
                        String sql = "insert into document_config_public_char(kind,type_name) values('语言类型','" + type_name + "')";
                        security_db.executeUpdate(sql);
                        String sql1 = "ALTER TABLE  document_multilanguage ADD " + type_name + "  varchar(200)  NOT NULL DEFAULT ''";
                        security_db.executeUpdate(sql1);
                        sql1 = "select column_group from security_publicconfig_key where tablename='document_multilanguage'";
                        rs = security_db.executeQuery(sql1);
                        if (rs.next()) {
                            String new_group = rs.getString("column_group") + "," + type_name;
                            sql1 = "update security_publicconfig_key set column_group='" + new_group + "' where tablename='document_multilanguage'";
                            security_db.executeUpdate(sql1);
                        }
                        String m = "init";
                        String sql2 = "select * from document_multilanguage where tablename!='erp' order by tablename";
                        ResultSet rs2 = security_db.executeQuery(sql2);
                        while (rs2.next()) {
                            if (!m.equals(rs2.getString("tablename"))) {
                                key = "multilanguage_" + m + "_" + type_name;
                                if (tt != null) {
                                    application.setAttribute(key, tt);
                                }
                                m = rs2.getString("tablename");
                                tt = new Hashtable();
                                if (rs2.getString(type_name).equals("")) {
                                    tt.put(rs2.getString("name"), rs2.getString("name"));
                                } else {
                                    tt.put(rs2.getString("name"), rs2.getString(type_name));
                                }
                            } else {
                                if (rs2.getString(type_name).equals("")) {
                                    tt.put(rs2.getString("name"), rs2.getString("name"));
                                } else {
                                    tt.put(rs2.getString("name"), rs2.getString(type_name));
                                }
                            }
                        }
                        key = "multilanguage_" + m + "_" + type_name;
                        if (tt != null) {
                            application.setAttribute(key, tt);
                        }
                        tt = new Hashtable();
                        sql2 = "select name," + type_name + " from document_multilanguage where tablename='erp'";
                        rs2 = security_db.executeQuery(sql2);
                        while (rs2.next()) {
                            if (rs2.getString(type_name).equals("")) {
                                tt.put(rs2.getString("name"), rs2.getString("name"));
                            } else {
                                tt.put(rs2.getString("name"), rs2.getString(type_name));
                            }
                        }
                        key = "multilanguage_erp_" + type_name;
                        application.setAttribute(key, tt);
                        response.sendRedirect("security/config/multilanguage/languageType_register_ok_b.jsp");
                    }
                } else {
                    response.sendRedirect("security/config/multilanguage/languageType_register_ok_c.jsp");
                }
                security_db.commit();
                security_db.close();
            } else {
                response.sendRedirect("error_conn.htm");
            }
        } catch (Exception ex) {
        }
    }
}
