/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package security.config.multilanguage;

import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.*;
import java.sql.*;
import java.io.*;
import include.nseer_db.*;

public class languageType_delete_ok extends HttpServlet {

    ServletContext application;
    HttpSession session;
    nseer_db_backup1 erp_db = null;

    public synchronized void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession dbSession = request.getSession();
        JspFactory _jspxFactory = JspFactory.getDefaultFactory();
        PageContext pageContext = _jspxFactory.getPageContext(this, request, response, "", true, 8192, true);
        ServletContext dbApplication = dbSession.getServletContext();
        try {
            HttpSession session = request.getSession();
            PrintWriter out = response.getWriter();
            nseer_db_backup1 security_db = new nseer_db_backup1(dbApplication);
            if (security_db.conn((String) dbSession.getAttribute("unit_db_name"))) {
                int i;
                int intRowCount;
                String sqll = "select * from document_config_public_char where kind='语言类型' order by type_ID";
                ResultSet rs = security_db.executeQuery(sqll);
                rs.next();
                rs.last();
                intRowCount = rs.getRow();
                String[] del = new String[intRowCount];
                String[] column = new String[intRowCount];
                del = (String[]) session.getAttribute("del");
                String new_group = "";
                if (del != null) {
                    for (i = 1; i <= intRowCount; i++) {
                        try {
                            if (del[i - 1] != null) {
                                String sql2 = "select * from document_config_public_char where id='" + del[i - 1] + "'";
                                ResultSet rs2 = security_db.executeQuery(sql2);
                                rs2.next();
                                column[i - 1] = rs2.getString("type_name");
                                new_group += "," + rs2.getString("type_name");
                                String sql3 = "ALTER TABLE document_multilanguage DROP " + column[i - 1];
                                security_db.executeUpdate(sql3);
                                String sql = "delete from document_config_public_char where id='" + del[i - 1] + "'";
                                security_db.executeUpdate(sql);
                            }
                        } catch (Exception ex) {
                            out.println("error" + ex);
                        }
                    }
                    if (!new_group.equals("")) {
                        String sql3 = "select column_group from security_publicconfig_key where tablename='document_multilanguage'";
                        rs = security_db.executeQuery(sql3);
                        if (rs.next()) {
                            String[] new_groups = new_group.substring(1).split(",");
                            new_group = rs.getString("column_group") + ",";
                            for (int j = 0; j < new_groups.length; j++) {
                                new_group = new_group.substring(0, new_group.indexOf("," + new_groups[j] + ",")) + new_group.substring(new_group.indexOf("," + new_groups[j] + ",") + new_groups[j].length() + 1);
                            }
                            new_group = new_group.substring(0, new_group.length() - 1);
                            sql3 = "update security_publicconfig_key set column_group='" + new_group + "' where tablename='document_multilanguage'";
                            security_db.executeUpdate(sql3);
                        }
                    }
                }
                security_db.commit();
                security_db.close();
            } else {
                response.sendRedirect("error_conn.htm");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        response.sendRedirect("security/config/multilanguage/languageType.jsp");
    }
}
