package security.config.key;

import include.nseer_db.nseer_db_backup1;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import include.nseer_db.*;
import include.tree_index.Nseer;

public class smtp_ok extends HttpServlet {
	public synchronized void service(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		Nseer n=new Nseer();
		String Mailhost=request.getParameter("Mailhost");
		String eName=request.getParameter("eName");
		String ePwd=request.getParameter("ePwd");
		nseer_db erpplatform_db = new nseer_db((String)session.getAttribute("unit_db_name"));
		String sql="select * from erpplatform_config_public_char where kind='mailhost'";
		ResultSet rs = erpplatform_db.executeQuery(sql);
		try {
			if(rs.next()){
				sql="update erpplatform_config_public_char set type_name='"+Mailhost+"',describe1='"+eName+"',describe2='"+n.E(ePwd)+"' where kind='mailhost'";
				erpplatform_db.executeUpdate(sql);
			}else{
				sql = "insert into erpplatform_config_public_char(kind,type_name,describe1,describe2) values ('mailhost','"+Mailhost+"','"+eName+"','"+n.E(ePwd)+"')";
				erpplatform_db.executeUpdate(sql);
			}
			erpplatform_db.close();
			response.sendRedirect("security/config/key/smtp_ok.jsp");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
