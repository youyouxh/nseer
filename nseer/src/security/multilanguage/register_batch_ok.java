/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package security.multilanguage;

import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.*;
import include.nseer_db.*;
import java.io.*;
import java.util.*;

public class register_batch_ok extends HttpServlet {

    public synchronized void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession dbSession = request.getSession();
        JspFactory _jspxFactory = JspFactory.getDefaultFactory();
        PageContext pageContext = _jspxFactory.getPageContext(this, request, response, "", true, 8192, true);
        ServletContext dbApplication = dbSession.getServletContext();
        ServletContext context = dbSession.getServletContext();
        try {
            nseer_db_backup1 security_db = new nseer_db_backup1(dbApplication);
            if (security_db.conn((String) dbSession.getAttribute("unit_db_name"))) {
                String id[] = request.getParameterValues("id");
                if (id != null) {
                    String describe[] = request.getParameterValues("describe");
                    String group_name[] = request.getParameterValues("group_name");
                    String name[] = request.getParameterValues("name");
                    String type = request.getParameter("type");
                    String multi = "";
                    Hashtable tt = null;
                    for (int i = 0; i < id.length; i++) {
                        String sql = "update document_multilanguage set " + type + "='" + describe[i] + "',tag='0' where id='" + id[i] + "'";
                        security_db.executeUpdate(sql);
                        if (!describe[i].equals("")) {
                            multi = "multilanguage_" + group_name[i] + "_" + type;
                            tt = (Hashtable) context.getAttribute(multi);
                            tt.remove(name[i]);
                            tt.put(name[i], describe[i]);
                            context.setAttribute(multi, tt);
                        }
                    }
                    response.sendRedirect("security/multilanguage/register_batch_ok_a.jsp");
                } else {
                    response.sendRedirect("security/multilanguage/register_batch_ok_b.jsp");
                }
                security_db.commit();
                security_db.close();
            } else {
                response.sendRedirect("error_conn.htm");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}