/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package security.multilanguage;

import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.*;
import java.sql.*;
import include.nseer_db.*;
import include.nseer_cookie.*;
import java.io.*;
import java.util.*;

public class register_list_ok extends HttpServlet {

    public synchronized void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession dbSession = request.getSession();
        JspFactory _jspxFactory = JspFactory.getDefaultFactory();
        PageContext pageContext = _jspxFactory.getPageContext(this, request, response, "", true, 8192, true);
        ServletContext dbApplication = dbSession.getServletContext();
        ServletContext context = dbSession.getServletContext();
        try {
            nseer_db_backup1 security_db = new nseer_db_backup1(dbApplication);
            if (security_db.conn((String) dbSession.getAttribute("unit_db_name"))) {
                String lang = "";
                String sql10 = "select * from document_config_public_char where kind='语言类型'";
                ResultSet rs10 = security_db.executeQuery(sql10);
                while (rs10.next()) {
                    lang += "," + rs10.getString("type_name");
                }
                String[] lang_group = lang.substring(1).split(",");
                String[] id_group = request.getParameterValues("id");
                String[] ori_name = request.getParameterValues("ori_name");
                String page = request.getParameter("curpage");
                String pageSize_temp = request.getParameter("curpagecount");
                page = page.split("⊙")[0];
                List<String[]> list = new ArrayList<String[]>();
                for (int i = 0; i < lang_group.length; i++) {
                    String[] lang_value = request.getParameterValues(lang_group[i]);
                    list.add(lang_value);
                }
                for (int i = 0; i < id_group.length; i++) {
                    String sql3 = "update document_multilanguage set ";
                    String sql4 = " where id='" + id_group[i] + "'";
                    String sql5 = "";
                    String multi = "";
                    Hashtable tt = null;
                    String[] value_group = new String[id_group.length];
                    for (int j = 0; j < list.size(); j++) {
                        value_group = (String[]) list.get(j);
                        if (!value_group[i].equals("")) {
                            multi = "multilanguage_erp_" + lang_group[j];
                            tt = (Hashtable) context.getAttribute(multi);
                            tt.remove(ori_name[i]);
                            tt.put(ori_name[i], value_group[i]);
                            context.setAttribute(multi, tt);
                        } else {
                            multi = "multilanguage_erp_" + lang_group[j];
                            tt = (Hashtable) context.getAttribute(multi);
                            tt.remove(ori_name[i]);
                            tt.put(ori_name[i], ori_name[i]);
                            context.setAttribute(multi, tt);
                        }
                        sql5 += "," + lang_group[j] + "='" + value_group[i] + "'";
                    }
                    sql3 = sql3 + sql5.substring(1) + sql4;
                    security_db.executeUpdate(sql3);
                }
                response.sendRedirect("security/multilanguage/register_list.jsp?strPage=" + page + "&pageSize=" + pageSize_temp);
                security_db.commit();
                security_db.close();
            } else {
                response.sendRedirect("error_conn.htm");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}