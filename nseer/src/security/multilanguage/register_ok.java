/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package security.multilanguage;

import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.*;
import java.sql.*;
import include.nseer_db.*;
import include.nseer_cookie.*;
import java.io.*;
import java.util.*;

public class register_ok extends HttpServlet {

    public synchronized void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession dbSession = request.getSession();
        JspFactory _jspxFactory = JspFactory.getDefaultFactory();
        PageContext pageContext = _jspxFactory.getPageContext(this, request, response, "", true, 8192, true);
        ServletContext dbApplication = dbSession.getServletContext();
        ServletContext context = dbSession.getServletContext();
        try {
            nseer_db_backup1 security_db = new nseer_db_backup1(dbApplication);
            if (security_db.conn((String) dbSession.getAttribute("unit_db_name"))) {
                String colname[] = request.getParameterValues("colname");
                String colvalue[] = request.getParameterValues("colvalue");
                String id = request.getParameter("id");
                String group_name = request.getParameter("group_name");
                String name = request.getParameter("name");
                String page = request.getParameter("page");
                int p = colvalue.length;
                String sql = "select * from document_multilanguage where language_tag='0'&&id='" + id + "'";
                ResultSet rs = security_db.executeQuery(sql);
                if (rs.next()) {
                    String sql3 = "update document_multilanguage set ";
                    String sql4 = "language_tag='0' where id='" + id + "'";
                    String sql5 = "";
                    for (int i = 0; i < colname.length; i++) {
                        sql5 = sql5 + colname[i] + "='" + colvalue[i] + "',";
                    }
                    sql3 = sql3 + sql5 + sql4;
                    security_db.executeUpdate(sql3);
                    String multi = "";
                    Hashtable tt = null;
                    for (int i = 0; i < colname.length; i++) {
                        if (!colvalue[i].equals("")) {
                            multi = "multilanguage_" + group_name + "_" + colname[i];
                            tt = (Hashtable) context.getAttribute(multi);
                            tt.remove(name);
                            tt.put(name, colvalue[i]);
                            context.setAttribute(multi, tt);
                        }
                    }
                    response.sendRedirect("security/multilanguage/register_ok_a.jsp?page=" + toUtf8String.utf8String(exchange.toURL(page)) + "");
                } else {
                    response.sendRedirect("security/multilanguage/register_ok_b.jsp?page=" + toUtf8String.utf8String(exchange.toURL(page)) + "");
                }
                security_db.commit();
                security_db.close();
            } else {
                response.sendRedirect("error_conn.htm");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}