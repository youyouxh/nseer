/*
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
package security.multilanguage;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.io.*;
import include.nseer_db.*;

public class delete_ok extends HttpServlet {

    ServletContext application;
    HttpSession session;
    nseer_db_backup1 erp_db = null;

    public synchronized void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession dbSession = request.getSession();
        JspFactory _jspxFactory = JspFactory.getDefaultFactory();
        PageContext pageContext = _jspxFactory.getPageContext(this, request, response, "", true, 8192, true);
        ServletContext dbApplication = dbSession.getServletContext();
        try {
            nseer_db_backup1 security_db = new nseer_db_backup1(dbApplication);
            PrintWriter out = response.getWriter();
            if (security_db.conn((String) dbSession.getAttribute("unit_db_name"))) {
                String id = request.getParameter("id");
                String sql2 = "delete from document_multilanguage where id='" + id + "'";
                security_db.executeUpdate(sql2);
                security_db.commit();
                security_db.close();
            } else {
                response.sendRedirect("error_conn.htm");
            }
        } catch (Exception ex) {
        }
    }
}