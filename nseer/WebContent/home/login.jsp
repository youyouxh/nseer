<!--
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 -->
<%@page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*,include.nseer_cookie.*,include.nseer_db.*" import="java.util.*" import="java.io.*" import="java.text.*"%>
<%@include file="head.jsp"%>
<HTML>
<jsp:useBean id="demo" class="include.tree_index.businessComment" scope="page"/>
<jsp:useBean id="lang" class="include.nseer_cookie.Lang" scope="page"/>
<%
Cookie cookies[]=request.getCookies();
String username="";
if(cookies!=null){
username=cookies[0].getValue();
if(username.length()==32) username="";
}


%>
<%
	 demo.setPath(request);
	 String language=request.getParameter("language");
	 if(language==null) language="chinese";
	 String unit_tag=request.getParameter("unit_tag");
	 if(unit_tag==null) unit_tag="0";
	 session.setAttribute("language",language);
%>
<HEAD>

<!-- <link href="../style/css/bs.css" rel="stylesheet" type="text/css"> -->
<img align="absmiddle" alt="" src="../images/empty.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/Lplus.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/close.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/L.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/Tminus.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/Lminus.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/I.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/P.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/Tplus.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/T.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/jsdoc.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/open.gif" border="0" style="display:none;"/>

<img align="absmiddle" alt="" src="../images/bg_0ltop.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_0rtop.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_01.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_03.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_0rtop.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_03.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/gb.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_04.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_0lbottom.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_02.gif" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/bg_0rbottom.gif" border="0" style="display:none;"/>

<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer30.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer16.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer12.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer13.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer32.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer33.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer26.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer31.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer20.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer16.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer25.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer18.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer24.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer5.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer15.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer17.png" border="0" style="display:none;"/>
<img align="absmiddle" alt="" src="../images/erpPlatform/design/nseer21.png" border="0" style="display:none;"/>
<TITLE><%=demo.getLang("erp","欢迎使用恩信科技开源ERP")%></TITLE>
<style type="text/css">

body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.STYLE1 {
	font-size: 13px;
	font-family: "宋体";
	color: #FFFFFF;
}
.STYLE2 {
	font-size: 13px;
	font-family: "宋体";
	color: #000000;
}
.STYLE3 {
	font-size: 13px;
	font-family: "宋体";
	color: #FFFFFF;
	line-height: 16pt;

}
.STYLE4 {
	font-size: 13px;
	font-family: "宋体";
	color: #244ac6;
	line-height: 16pt;
}
.STYLE5 {
	font-size: 13px;
	font-family: "宋体";
	color: #000000;
	line-height: 16pt;
}
</style>
<link rel="stylesheet" type="text/css" href="../css/include/nseer_cookie/xml-css.css"/>
<style type="text/css" media="screen">@import url(niceforms-default.css);</style>
<script>
/*#############################################################
Name: Niceforms
Version: 1.0
Author: Lucian Slatineanu
URL: http://www.badboy.ro/

Feel free to use and modify but please provide credits.
#############################################################*/

//Global Variables
var niceforms = document.getElementsByTagName('form'); 
var inputs = new Array();
var labels = new Array(); 
var radios = new Array(); 
var radioLabels = new Array();
var checkboxes = new Array(); 
var checkboxLabels = new Array();
var texts = new Array();
var textareas = new Array();
var selects = new Array();
var selectText = "please select";
var agt = navigator.userAgent.toLowerCase(); 
this.ie = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1)); 
var hovers = new Array();
var buttons = new Array(); 
var isMac = new RegExp('(^|)'+'Apple'+'(|$)');

//Theme Variables - edit these to match your theme

var imagesPath = "images/";

//Initialization function - if you have any other 'onload' functions, add them here
function init() {
	if(!document.getElementById) {return false;}
	preloadImages();
	getElements();
	separateElements();
	
	
	if(!isMac.test(navigator.vendor)) {
		replaceTexts();
		replaceTextareas();
		buttonHovers();
	}
}
//preloading required images
function preloadImages() {
	preloads = new Object();
	preloads[0] = new Image(); preloads[0].src = imagesPath + "button_left_xon.gif";
	preloads[1] = new Image(); preloads[1].src = imagesPath + "button_right_xon.gif";
	preloads[2] = new Image(); preloads[2].src = imagesPath + "input_left_xon.gif";
	preloads[3] = new Image(); preloads[3].src = imagesPath + "input_right_xon.gif";
	preloads[4] = new Image(); preloads[4].src = imagesPath + "txtarea_bl_xon.gif";
	preloads[5] = new Image(); preloads[5].src = imagesPath + "txtarea_br_xon.gif";
	preloads[6] = new Image(); preloads[6].src = imagesPath + "txtarea_cntr_xon.gif";
	preloads[7] = new Image(); preloads[7].src = imagesPath + "txtarea_l_xon.gif";
	preloads[8] = new Image(); preloads[8].src = imagesPath + "txtarea_tl_xon.gif";
	preloads[9] = new Image(); preloads[9].src = imagesPath + "txtarea_tr_xon.gif";
}
//getting all the required elements
function getElements() {
	var re = new RegExp('(^| )'+'niceform'+'( |$)');
	for (var nf = 0; nf < document.getElementsByTagName('form').length; nf++) {
		if(re.test(niceforms[nf].className)) {
			for(var nfi = 0; nfi < document.forms[nf].getElementsByTagName('input').length; nfi++) {inputs.push(document.forms[nf].getElementsByTagName('input')[nfi]);}
		 if (window.ActiveXObject)
			{
for(var nft = 0; nft < document.forms[nf].getElementsByTagName('textarea').length; nft++) {textareas.push(document.forms[nf].getElementsByTagName('textarea')[nft]);}		
		}
		}
	}
}
//separating all the elements in their respective arrays
function separateElements() {
	var r = 0; var c = 0; var t = 0; var rl = 0; var cl = 0; var tl = 0; var b = 0;
	for (var q = 0; q < inputs.length; q++) {
		//if((inputs[q].type == "text") || (inputs[q].type == "password")) {texts[t] = inputs[q]; ++t;}
		if((inputs[q].type == "submit") || (inputs[q].type == "button")|| (inputs[q].type == "reset")) {buttons[b] = inputs[q]; ++b;}
	}
}
function replaceTexts() {
	for(var q = 0; q < texts.length; q++) {
		texts[q].style.width = texts[q].size * 10 + 'px';
		txtLeft = document.createElement('img'); txtLeft.src = imagesPath + "input_left.gif"; txtLeft.className = "inputCorner";
		txtRight = document.createElement('img'); txtRight.src = imagesPath + "input_right.gif"; txtRight.className = "inputCorner";
		texts[q].parentNode.insertBefore(txtLeft, texts[q]);
		texts[q].parentNode.insertBefore(txtRight, texts[q].nextSibling);
		texts[q].className = "textinput";
		//create hovers
		texts[q].onfocus = function() {
			this.className = "textinputHovered";
			this.previousSibling.src = imagesPath + "input_left_xon.gif";
			this.nextSibling.src = imagesPath + "input_right_xon.gif";
		}
		texts[q].onblur = function() {
			this.className = "textinput";
			this.previousSibling.src = imagesPath + "input_left.gif";
			this.nextSibling.src = imagesPath + "input_right.gif";
		}
	}
}
function replaceTextareas() {
	for(var q = 0; q < textareas.length; q++) {
		var where = textareas[q].parentNode;
		var where2 = textareas[q].previousSibling;
		textareas[q].style.width = textareas[q].cols * 10 + 'px';
		textareas[q].style.height = textareas[q].rows * 10 + 'px';
		//create divs
		var container = document.createElement('div');
		container.className = "txtarea";
		container.style.width = textareas[q].cols * 10 + 20 + 'px';
		container.style.height = textareas[q].rows * 10 + 20 + 'px';
		var topRight = document.createElement('div');
		topRight.className = "tr";
		var topLeft = document.createElement('img');
		topLeft.className = "txt_corner";
		topLeft.src = imagesPath + "txtarea_tl.gif";
		var centerRight = document.createElement('div');
		centerRight.className = "cntr";
		var centerLeft = document.createElement('div');
		centerLeft.className = "cntr_l";
		if(!this.ie) {centerLeft.style.height = textareas[q].rows * 10 + 10 + 'px';}
		else {centerLeft.style.height = textareas[q].rows * 10 + 12 + 'px';}
		var bottomRight = document.createElement('div');
		bottomRight.className = "br";
		var bottomLeft = document.createElement('img');
		bottomLeft.className = "txt_corner";
		bottomLeft.src = imagesPath + "txtarea_bl.gif";
		//assemble divs
		container.appendChild(topRight);
		topRight.appendChild(topLeft);
		container.appendChild(centerRight);
		centerRight.appendChild(centerLeft);
		centerRight.appendChild(textareas[q]);
		container.appendChild(bottomRight);
		bottomRight.appendChild(bottomLeft);
		//insert structure
		where.insertBefore(container, where2);
		//create hovers
		textareas[q].onfocus = function() {
			this.previousSibling.className = "cntr_l_xon";
			this.parentNode.className = "cntr_xon";
			this.parentNode.previousSibling.className = "tr_xon";
			this.parentNode.previousSibling.getElementsByTagName("img")[0].src = imagesPath + "txtarea_tl_xon.gif";
			this.parentNode.nextSibling.className = "br_xon";
			this.parentNode.nextSibling.getElementsByTagName("img")[0].src = imagesPath + "txtarea_bl_xon.gif";
		}
		textareas[q].onblur = function() {
			this.previousSibling.className = "cntr_l";
			this.parentNode.className = "cntr";
			this.parentNode.previousSibling.className = "tr";
			this.parentNode.previousSibling.getElementsByTagName("img")[0].src = imagesPath + "txtarea_tl.gif";
			this.parentNode.nextSibling.className = "br";
			this.parentNode.nextSibling.getElementsByTagName("img")[0].src = imagesPath + "txtarea_bl.gif";
		}
	}
}
function buttonHovers() {
	for (var i = 0; i < buttons.length; i++) {
		buttons[i].className = "buttonSubmit";
		var buttonLeft = document.createElement('img');
		buttonLeft.src = imagesPath + "button_left.gif";
		buttonLeft.className = "buttonImg";
		buttons[i].parentNode.insertBefore(buttonLeft, buttons[i]);
		var buttonRight = document.createElement('img');
		buttonRight.src = imagesPath + "button_right.gif";
		buttonRight.className = "buttonImg";
		if(buttons[i].nextSibling) {buttons[i].parentNode.insertBefore(buttonRight, buttons[i].nextSibling);}
		else {buttons[i].parentNode.appendChild(buttonRight);}
		buttons[i].onmouseover = function() {
			this.className += "Hovered";
			this.previousSibling.src = imagesPath + "button_left_xon.gif";
			this.nextSibling.src = imagesPath + "button_right_xon.gif";
		}
		buttons[i].onmouseout = function() {
			this.className = this.className.replace(/Hovered/g, "");
			this.previousSibling.src = imagesPath + "button_left.gif";
			this.nextSibling.src = imagesPath + "button_right.gif";
		}
	}
}
//Useful functions
function findPosY(obj) {
	var posTop = 0;
	while (obj.offsetParent) {posTop += obj.offsetTop; obj = obj.offsetParent;}
	return posTop;
}
function findPosX(obj) {
	var posLeft = 0;
	while (obj.offsetParent) {posLeft += obj.offsetLeft; obj = obj.offsetParent;}
	return posLeft;
}
</script>
<script language="javascript" src="../javascript/input_control/Check.js"></script>
<script language="javascript">
function CheckForm(TheForm) {
	if (TheForm.username.value == "") {
		alert("<%=demo.getLang("erp","请填写您的用户名！")%>");
		TheForm.username.focus();
		return false;
	}
    if (TheForm.passwd.value == "") {
		alert("<%=demo.getLang("erp","请填写您的密码！")%>");
		TheForm.passwd.focus();
		return false;
	}
	if (TheForm.rand.value == "") {
		alert("<%=demo.getLang("erp","请填写正确的验证码！")%>");
		TheForm.rand.focus();
		return false;
	}
	return true;
}

function send(option){
	var a=option.options[option.selectedIndex].value;
	location.href=a;
}

function login(){
	var login=document.getElementById("login");
	
	login.style.display=login.style.display=='block'?'none':'block';
 var log=document.getElementById("log");
	
	log.style.display=login.style.display=='block'?'none':'block';
 
}

function save(title, url){
if (document.all)
window.external.AddFavorite(url, title);
else if (window.sidebar)
window.sidebar.addPanel(title, url, "")
}

function setHomePage()
{
  if(window.netscape)
  {
        try { 
          netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect"); 
        } 
        catch (e) 
        { 
          alert("此操作被浏览器拒绝！\n请在浏览器地址栏输入“about:config”并回车\n然后将[signed.applets.codebase_principal_support]设置为'true'"); 
        }
  

  var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
  prefs.setCharPref('browser.startup.homepage',window.location.href);
  }else{
  this.homepage.style.behavior='url(#default#homepage)';this.homepage.sethomepage(window.location.href);
  }

}


</script>
<script type="text/javascript">
function call_image(){
    var img = document.getElementById("image");
    img.src = "image.jsp?" + Math.random();
}
</script>
<%
ServletContext context=session.getServletContext();
String path=context.getRealPath("/");
String[] languages=lang.getLangs("ondemand1");
%>
<div>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" readonly>
<tr>
<td colspan="2" height="10%" width="100%">
<div style="width:100%;height:100%;FILTER: progid:DXImageTransform.Microsoft.Gradient(startColorStr='#C7E0F6', endColorStr='#ffffff', gradientType='0');">
<table width="100%" height="100%">
<tr>
<td style="vertical-align:bottom;"  height="44" width="17%"><a href="#" target="_blank" title="<%=demo.getLang("erp","访问")%><%=demo.getLang("erp","恩信科技")%><%=demo.getLang("erp","网站")%>"><img src="../images/main/0427.gif"/></a>&nbsp;</td>
<td style="vertical-align:bottom;"  width="68%">&nbsp;</td>
<td style="vertical-align:bottom;padding:9px;" width="15%" align="right"><a href="user_enrollment.jsp"><font color="#000000"><%=demo.getLang("erp","添加用户")%></font></a></td>
</tr></table>
</div>
</td></tr>

<tr>
<td width="35%" align="right" style="padding-right:5px;padding-top:20px">
<div id="logDiv" style="width:310px;height:413px">
<TABLE width="100%" height="100%" border="0" align="center" cellPadding="0" cellSpacing="0">
  <TBODY>
    <TR>
      <TD width="1%" height="1%"><IMG  src="../images/bg_0ltop.gif" /></TD>
      <TD width="100%" background="../images/bg_01.gif"></TD>
      <TD width="1%" height="1%"><IMG  src="../images/bg_0rtop.gif" /></TD>
    </TR>
    <TR>
      <TD  background="../images/bg_03.gif"></TD>
 <TD style="background:f0f0f0" align="center" width="100%">
<form name="TheForm" method="post" action="../work" onSubmit="return CheckForm(this)">
<input type="hidden" name="ui" value="1">
<table border="0">
<tr>
<td colspan="2"><%=demo.getLang("erp","欢迎登录恩信科技开源")%>ERP V7.19</td>
</tr>
<tr>
<%
nseer_db db=new nseer_db("mysql");
String sql="select * from unit_info where active_tag='1'";
ResultSet rs=db.executeQuery(sql);
if(rs.next()){
	if(unit_tag.equals("0")){
%>
<tr>
<td height="25"><font color="#000000"><%=demo.getLang("erp","单位")%>:&nbsp;</td><td><%=rs.getString("unit_name")%></td>
</tr>
<%}else{%>
<tr>
<td height="25"><font color="#000000"><%=demo.getLang("erp","单位")%>:&nbsp;</td><td><input type="text"  name="unit_name" id="unit_name" style="width:140" value="<%=rs.getString("unit_name")%>"></td>
</tr>
<%}}else{%>
<tr>
<td height="25"><font color="#000000"><%=demo.getLang("erp","单位")%>:&nbsp;</td><td><input type="text"  name="unit_name" id="unit_name" style="width:140" value="恩信科技ERP用户"></td>
</tr>
<%}
db.close();
%>
<td ><font color="#000000">
<%=demo.getLang("erp","语言")%>:</td><td><select name="language" style="width:140" onChange="send(this)">
<%for(int i=0;i<languages.length;i++){%>
<option value="login.jsp?language=<%=languages[i]%>"  <%=language.equals(languages[i])?"selected":""%>><%=languages[i]%></option>
<%}%>
</select><br></font></td>
</tr>
<tr>
<td><font color="#000000"><%=demo.getLang("erp","用户")%>:&nbsp;</td><td><input type="text"  name="username" id="username" style="width:140" value="<%=username%>"><br></font></td>

<td><input type="checkbox" name="remember" value="1"/><%=demo.getLang("erp","记住")%></td></tr>
<tr>
<td><font color="#000000"><%=demo.getLang("erp","密码")%>:&nbsp;&nbsp;</b></td><td><input type="password" name="passwd" id="passwd" value="" style="width:140"><br></font></td></tr>
<tr>
<tr>
<td ><%=demo.getLang("erp","验证码")%>:&nbsp;&nbsp;</td><td><input style="width:70" type="text" name="rand" >&nbsp;&nbsp;&nbsp;<img id="image" title="<%=demo.getLang("erp","换一张")%>" src="image.jsp?Math.radom();" onclick="return call_image();"></td><td>&nbsp;&nbsp;<a href="javascript:call_image();"><font color="#000000"><%=demo.getLang("erp","换一张")%></font></a></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td colspan="2" align="center">
<input type="submit" class=btn1_mouseout onmouseover="this.className='btn1_mouseover'"
onmouseout="this.className='btn1_mouseout'" value="<%=demo.getLang("erp","登录")%>">&nbsp;&nbsp;<input type="button" class=btn1_mouseout onmouseover="this.className='btn1_mouseover'"
onmouseout="this.className='btn1_mouseout'"  value="<%=demo.getLang("erp","修改密码")%>" onClick=location="change_passwd.jsp?language=<%=language%>"><br></td></tr>
<tr>
<td colspan="3">&nbsp;</td></tr>
<tr>
<td colspan="3"><p><a href="#" name="homepage" target="_search" onclick="setHomePage()"><font color="#000000"><%=demo.getLang("erp","设为首页")%></a>&nbsp;|&nbsp;<a href="javascript:save('#','#')"><font color="#000000"><%=demo.getLang("erp","加入收藏夹")%></a>&nbsp;|&nbsp;<a href="reset_passwd.jsp?language=<%=language%>"><font color="#000000"><%=demo.getLang("erp","忘记密码")%></a>&nbsp;|&nbsp;<a href="login.jsp?language=<%=language%>&unit_tag=1"><font color="#000000"><%=demo.getLang("erp","修改单位")%></a></td></font></p></tr></table>
</form>
 </TD>
<TD  background="../images/bg_04.gif"></TD>
    </TR>
    <TR>
      <TD width="1%" height="1%"><IMG  src="../images/bg_0lbottom.gif" /></TD>
      <TD background="../images/bg_02.gif"></TD>
      <TD width="1%" height="1%"><IMG  src="../images/bg_0rbottom.gif" /></TD>
    </TR>    
  </TBODY>
</TABLE>
</div>
</td>

<td width="100%" align="left" style="padding-left:5px;padding-top:20px">
<div id="pressDiv" style="width:670px;height:413px;">
<TABLE width="100%" height="100%" border="0" align="center" cellPadding="0" cellSpacing="0">
  <TBODY>
    <TR>
      <TD width="1%" height="1%"><IMG  src="../images/bg_0ltop.gif" /></TD>
      <TD width="100%" background="../images/bg_01.gif"></TD>
      <TD width="1%" height="1%"><IMG  src="../images/bg_0rtop.gif" /></TD>
    </TR>
    <TR>
      <TD  background="../images/bg_03.gif"></TD>
 <TD style="background:e5e5e5">

<div style="width:100%;height:100%;background:#E8E8E8">
<table id="table1" class="nseer_table1" width="100%" height="100%" border="0" cellspacing="1" cellpadding="1" >
	<tr height="20%">
	<td>
	<script>
var widths='660'; 
var heights='393'; 
var counts=6; 
img1=new Image ();img1.src='../images/home/login1.jpg';
img2=new Image ();img2.src='../images/home/login5.jpg';
img3=new Image ();img3.src='../images/home/login3.jpg'; 
img4=new Image ();img4.src='../images/home/login9.jpg'; 
img5=new Image ();img5.src='../images/home/login6.jpg';
img6=new Image ();img6.src='../images/home/login2.jpg';
// url1=new Image ();url1.src='http://www.nseer.com'; 
// url2=new Image ();url2.src='http://www.nseer.com'; 
// url3=new Image ();url3.src='http://www.nseer.com'; 
// url4=new Image ();url4.src='http://www.nseer.com'; 
// url5=new Image ();url5.src='http://www.nseer.com';
// url6=new Image ();url6.src='http://www.nseer.com';  
var imageItem=[
'<%=demo.getLang("erp","自助实施")%>',
'<%=demo.getLang("erp","VIP")%>',
'<%=demo.getLang("erp","加盟")%>',
'<%=demo.getLang("erp","高端培训")%>',
'<%=demo.getLang("erp","云计算")%>',
'<%=demo.getLang("erp","荣誉")%>'
];
var nn=1; 
var key=0; 
function change_img() 
{if(key==0){key=1;} 
else if(document.all) 
{document.getElementById("pic").filters[0].Apply();document.getElementById("pic").filters[0].Play(duration=2);} 
eval('document.getElementById("pic").src=img'+nn+'.src'); 
eval('document.getElementById("url").href=url'+nn+'.src'); 
for (var i=1;i<=counts;i++){document.getElementById("xxjdjj"+i).className='axx';} 
document.getElementById("xxjdjj"+nn).className='bxx'; 
nn++;if(nn>counts){nn=1;} 
tt=setTimeout('change_img()',12000);} 
function changeimg(n){nn=n;window.clearInterval(tt);change_img();} 
document.write('<style>'); 
document.write('.axx{padding:1px 7px;border-left:#cccccc 1px solid;}'); 
document.write('a.axx:link,a.axx:visited{text-decoration:none;color:#fff;line-height:16px;font:12px sans-serif;background-color:#666;}'); 
document.write('a.axx:active,a.axx:hover{text-decoration:none;color:#fff;line-height:16px;font:12px sans-serif;background-color:#999;}'); 
document.write('.bxx{padding:1px 7px;border-left:#cccccc 1px solid;}'); 
document.write('a.bxx:link,a.bxx:visited{text-decoration:none;color:#fff;line-height:16px;font:12px sans-serif;background-color:#D34600;}'); 
document.write('a.bxx:active,a.bxx:hover{text-decoration:none;color:#fff;line-height:16px;font:12px sans-serif;background-color:#D34600;}'); 
document.write('</style>'); 
document.write('<div style="width:'+widths+'px;height:'+heights+'px;overflow:hidden;text-overflow:clip;">'); 
document.write('<div><a id="url" target="_blank" title="<%=demo.getLang("erp","访问")%><%=demo.getLang("erp","恩信科技")%><%=demo.getLang("erp","网站")%>"><img id="pic" style="border:0px;filter:progid:dximagetransform.microsoft.wipe(gradientsize=1.0,wipestyle=4, motion=forward)" width="'+widths+'"px height="'+heights+'"px /></a></div>'); 
document.write('<div style="filter:alpha(style=1,opacity=10,finishOpacity=80);width:100%;text-align:right;top:-14px;position:relative;margin:1px;height:14px;padding:0px;margin:0px;border:0px;">'); 
for(var i=1;i<counts+1;i++){document.write('<a href="javascript:changeimg('+i+');" id="xxjdjj'+i+'" class="axx" target="_self">'+imageItem[i-1]+'</a>');} 
document.write('</div></div>'); 
change_img(); 
</script>
	</td>
	</tr>
</table>
</div>
</TD>
<TD  background="../images/bg_04.gif"></TD>
    </TR>
    <TR>
      <TD width="1%" height="1%"><IMG  src="../images/bg_0lbottom.gif" /></TD>
      <TD background="../images/bg_02.gif"></TD>
      <TD width="1%" height="1%"><IMG  src="../images/bg_0rbottom.gif" /></TD>
    </TR>    
  </TBODY>
</TABLE>
</div>
</td>
</tr>

<tr>
<td colspan="2" height="10%" width="100%">
<div style="width:100%;height:100%;FILTER: progid:DXImageTransform.Microsoft.Gradient(startColorStr='#ffffff', endColorStr='#C7E0F6', gradientType='0');">
<table width="100%" height="40%">
        <tr>
          <td <%=TD_STYLE3%> class="TD_STYLE3" align="center"><span onclick="window.open('http://www.nseer.com')" style="cursor:pointer;" title="<%=demo.getLang("erp","访问")%><%=demo.getLang("erp","恩信科技")%><%=demo.getLang("erp","网站")%>"><%=demo.getLang("erp","北京恩信创业科技有限公司")%>&nbsp;<%=demo.getLang("erp","版权所有")%></span>
          </td>
        </tr>
</table>  
</div>     
</td>
</tr>
</table>
</div>
<%@include file="foot.htm"%>