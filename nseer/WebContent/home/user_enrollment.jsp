<!--
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 -->
<%@page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*,include.nseer_cookie.*" import="java.util.*" import="java.io.*" import="java.text.*"%>
<jsp:useBean id="demo" class="include.tree_index.businessComment" scope="page" />
<script language="javascript" src="../javascript/ajax/ajax-validation4.js"></script>
<script type="text/javascript" src="../javascript/include/div/divcolor.js"></script>
<%
demo.setPath(request);
String language=request.getParameter("language");
if(language==null) language="chinese";
session.setAttribute("language",language);
%>
<%@include file="head.jsp"%>
<link rel="stylesheet" type="text/css" href="../javascript/ajax/niceforms-default.css" />
<script type="text/javascript" src="../javascript/ajax/niceforms.js"></script> 
<TABLE width="80%" height="90%" border=0 cellPadding=0 cellSpacing=0 align="center" bgcolor="#F3F6F7" class="example" id="example1">
<script>
setGradient('example1','#A5E3FF','#ffffff',0);
</script>
<TBODY>
<TR>
<TD width="1%" height="1%"><IMG src="../images/bg_0ltop.gif" ></TD>
<TD width="100%" background="../images/bg_01.gif"></TD>
<TD width="1%" height="1%"><IMG src="../images/bg_0rtop.gif"></TD>
</TR>
<TR>
<TD background="../images/bg_03.gif"></TD>
<TD>
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2">
<tr>
<td <%=TD_STYLE3%> class="TD_STYLE3" height="80"></td>
</tr>
</table>
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2" width="650">
<tr>
<td <%=TD_STYLE3%> class="TD_STYLE3">
<p align="center"><b><font color="#800000" size="3"><%=demo.getLang("erp","恩信科技ERP软件添加用户")%></font></b></td>
</tr>
</table>
<script language="javascript">
function setfocus(){
document.form1.lica.focus();
}
function b1keyup(){
if(document.form1.lica.value.length==5){
document.form1.licb.focus();
}
}
function b2keyup(){
if(document.form1.licb.value.length==5){
document.form1.licc.focus();
}
}
function b3keyup(){
if(document.form1.licc.value.length==5){
document.form1.licd.focus();
}
}
function b4keyup(){
if(document.form1.licd.value.length==5){
document.form1.realname.focus();
}
}
</script>
<script language="javascript" src="../javascript/input_control/Check.js"></script>
<script language="javascript">
var count=0;
function CheckForm(TheForm){
trimform(TheForm);
if(count>0){
return false;
}
count++;
if (TheForm.lica.value==""){
alert("<%=demo.getLang("erp","请填写您的许可证号！")%>");
TheForm.name.focus();
count=count-1;
return false;
}
if (TheForm.licb.value==""){
alert("<%=demo.getLang("erp","请填写您的许可证号！")%>");
TheForm.name.focus();
count=count-1;
return false;
}
if (TheForm.licc.value==""){
alert("<%=demo.getLang("erp","请填写您的许可证号！")%>");
TheForm.name.focus();
count=count-1;
return false;
}
if (TheForm.licd.value==""){
alert("<%=demo.getLang("erp","请填写您的许可证号！")%>");
TheForm.name.focus();
count=count-1;
return false;
}
if (TheForm.realname.value==""){
alert("<%=demo.getLang("erp","请填写您的用户姓名！")%>");
TheForm.name.focus();
count=count-1;
return false;
}
if (TheForm.name.value==""){
alert("<%=demo.getLang("erp","请填写您的用户名！")%>");
TheForm.name.focus();
count=count-1;
return false;
}
if (TheForm.pwd.value==""){
alert("<%=demo.getLang("erp","请填写您的密码！")%>");
TheForm.pwd.focus();
count=count-1;
return false;
}
if (TheForm.pwd.value !=TheForm.passwd.value){
alert("<%=demo.getLang("erp","请正确填写您的新密码！")%>");
TheForm.passwd.focus();
count=count-1;
return false;
}
if (TheForm.question.value==""){
alert("<%=demo.getLang("erp","请填写确认身份的问题！")%>");
TheForm.question.focus();
count=count-1;
return false;
}
if (TheForm.answer.value==""){
alert("<%=demo.getLang("erp","请填写确认身份的答案！")%>");
TheForm.answer.focus();
count=count-1;
return false;
}
return true;
}
</script>
<form id="userName" class="x-form" name="form1" method="POST" action="user_enrollment_ok.jsp?language=<%=language%>" onSubmit="return CheckForm(this)">
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2" width="650">
<tr>
<td <%=TD_STYLE3%> class="TD_STYLE3">&nbsp;</td>
</tr>
</table>
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2" width="650">
<tr>
<td <%=TD_STYLE3%> class="TD_STYLE3">&nbsp;</td>
</tr>
</table>
<table <%=TABLE_STYLE1%> class="TABLE_STYLE1" style="width:65%" align="center">
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="30%"><font color=red>*</font><%=demo.getLang("erp","用户许可证号码")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="70%" colspan="2"><input type="text" <%=INPUT_STYLE1%> class="INPUT_STYLE1" name="lica" style="width:12%" onkeyup="return b1keyup()">-<input type="text" <%=INPUT_STYLE1%> class="INPUT_STYLE1" name="licb" style="width:12%" onkeyup="return b2keyup()">-<input type="text" <%=INPUT_STYLE1%> class="INPUT_STYLE1" name="licc" style="width:12%" onkeyup="return b3keyup()">-<input type="text" <%=INPUT_STYLE1%> class="INPUT_STYLE1" name="licd" style="width:12%" onkeyup="return b4keyup()">&nbsp;&nbsp;<span onmouseover="javascript:showDiv();" onmouseout="javascript:closeDiv();" style="cursor:pointer"><font color="red"><%=demo.getLang("erp","如何获得")%><%=demo.getLang("erp","用户许可证号码")%></font></span></td>
</tr>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="30%"><font color=red>*</font><%=demo.getLang("erp","使用单位简称")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="70%" colspan="2"><input type="text" <%=INPUT_STYLE1%> class="INPUT_STYLE1" name="unit_name" style="width:33%" value="nseer" id="nseer_name" onFocus="this.blur()"></td>
</tr>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="30%"><font color=red>*</font><%=demo.getLang("erp","用户姓名")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="70%" colspan="2"><input type="text" <%=INPUT_STYLE1%> class="INPUT_STYLE1" name="realname" style="width:33%"></td>
</tr>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="30%"><font color=red>*</font><%=demo.getLang("erp","用户名")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="30%"><input type="text" <%=INPUT_STYLE1%> class="INPUT_STYLE1" id="validator_dup" name="name" style="width:100%" onblur="ajax_validation('userName','nseer_name','validator_dup','security_users','name','../vd5',this)"></td><td <%=TD_STYLE21%> class="TD_STYLE2">&nbsp;<span onmouseover="javascript:showDiv_user();" onmouseout="javascript:closeDiv_user();" style="cursor:pointer;"><font color="red"><%=demo.getLang("erp","如何定义")%><%=demo.getLang("erp","用户名")%></font></span>&nbsp;<span id="loaddiv" style="display:none;width:50%;text-align:center;background:#ff0033;"></span></td>
</tr>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="30%"><font color=red>*</font><%=demo.getLang("erp","密码")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="70%" colspan="2"><input type="password" name="passwd" style="background-color: #ffffff" style="width:33%"></td>
</tr>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="30%"><font color=red>*</font><%=demo.getLang("erp","确认密码")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="70%" colspan="2"><input type="password" name="pwd" style="background-color: #ffffff" style="width:33%"></td>
</tr>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="30%"><font color=red>*</font><%=demo.getLang("erp","忘记密码时确认身份的问题 ")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="70%" colspan="2"><select <%=SELECT_STYLE1%> class="SELECT_STYLE1" name="question" style="width:33%">
<option value="您最喜欢的颜色？"><%=demo.getLang("erp","您最喜欢的颜色？")%></option>
<option value="您最喜欢的食物？"><%=demo.getLang("erp","您最喜欢的食物？")%></option>
<option value="您最喜欢的运动？"><%=demo.getLang("erp","您最喜欢的运动？")%></option>
<option value="您宠物的名字？"><%=demo.getLang("erp","您宠物的名字？")%></option>
<option value="您母亲的姓氏？"><%=demo.getLang("erp","您母亲的姓氏？")%></option>
<option value="您出生的城市？"><%=demo.getLang("erp","您出生的城市？")%></option>
</select></td>
</tr>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="30%"><font color=red>*</font><%=demo.getLang("erp","忘记密码时确认身份的答案")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="70%" colspan="2"><input type="text" name="answer" <%=INPUT_STYLE1%> class="INPUT_STYLE1" style="width:33%"></td>
</tr>
</table> 
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2" width="650" height="10">
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE3%> class="TD_STYLE3" height="20"></td>
</tr>
</table>
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2" width="650" height="10">
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE3%> class="TD_STYLE3"><p align="center"><input type="submit" <%=SUBMIT_STYLE1%> class="SUBMIT_STYLE1" value="<%=demo.getLang("erp","注册")%>" name="B1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" <%=BUTTON_STYLE1%> class="BUTTON_STYLE1" value="<%=demo.getLang("erp","返回")%>" onClick=location="login.jsp"></td> 
</tr>
</table> 
</form> 
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2" width="650" height="10">
<tr>
<td <%=TD_STYLE3%> class="TD_STYLE3" height="200"></td>
</tr>
</table>
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2" width="80%" height="83">
<tr>
<td <%=TD_STYLE3%> class="TD_STYLE3" height="79" align="center"><%=demo.getLang("erp","北京恩信创业科技有限公司版权所有 All Rights Reserved.")%></td>
</tr>
</table> 
<%@include file="foot.htm"%>
<TD background="../images/bg_04.gif"></TD>
</TR>
<TR>
<TD width="1%" height="1%"><IMG src="../images/bg_0lbottom.gif" ></TD>
<TD background="../images/bg_02.gif"></TD>
<TD width="1%" height="1%"><IMG src="../images/bg_0rbottom.gif"></TD>
</TR>
</TBODY>
</TABLE>
<div id="license_div" style="position:absolute;top:0px;right:100px;width:400px;height:100px;display:none">
<TABLE width="100%" height="100%" border="0" align="center" cellPadding="0" cellSpacing="0"><TBODY><TR><TD width="1%" height="1%"><IMG  src="../images/bg_0ltop.gif"></TD><TD width="100%" background="../images/bg_01.gif"></TD><TD width="1%" height="1%"><IMG  src="../images/bg_0rtop.gif"></TD></TR><TR><TD  background="../images/bg_03.gif"></TD><TD><div style="position:absolute;top:10px;width:380px;height:80px;background:#F0F3F5;"><div style="height:50px;text-align:left;color:#0000FF;line-height:150%;padding-top:7px;padding-left:10px;padding-right:10px;">为保障使用单位安全应用本系统，新用户需向使用单位系统管理员申请用户许可证。使用单位系统管理员登录本系统后可以为新用户发放包含使用权限信息的用户许可证。</div></div></div></div> </TD><TD  background="../images/bg_04.gif"></TD></TR><TR><TD width="1%" height="1%"><IMG  src="../images/bg_0lbottom.gif" ></TD><TD background="../images/bg_02.gif"></TD><TD width="1%" height="1%"><IMG  src="../images/bg_0rbottom.gif"></TD> </TR></TBODY></TABLE>
</div>
<div id="user_div" style="position:absolute;top:0px;right:100px;width:400px;height:100px;display:none">
<TABLE width="100%" height="100%" border="0" align="center" cellPadding="0" cellSpacing="0"><TBODY><TR><TD width="1%" height="1%"><IMG  src="../images/bg_0ltop.gif"></TD><TD width="100%" background="../images/bg_01.gif"></TD><TD width="1%" height="1%"><IMG  src="../images/bg_0rtop.gif"></TD></TR><TR><TD  background="../images/bg_03.gif"></TD><TD><div style="position:absolute;top:10px;width:380px;height:80px;background:#F0F3F5;"><div style="height:50px;text-align:left;color:#0000FF;line-height:150%;padding-top:7px;padding-left:10px;padding-right:10px;">该用户名格式为：使用单位简称_自定义字符串，例如：nseer_admin。建议自定义字符串由字母和数字组成。</div></div></div></div> </TD><TD  background="../images/bg_04.gif"></TD></TR><TR><TD width="1%" height="1%"><IMG  src="../images/bg_0lbottom.gif" ></TD><TD background="../images/bg_02.gif"></TD><TD width="1%" height="1%"><IMG  src="../images/bg_0rbottom.gif"></TD> </TR></TBODY></TABLE>
</div>
<script>
function showDiv(){
	document.getElementById('license_div').style.display='block';
}
function closeDiv(){
	document.getElementById('license_div').style.display='none';
}
function showDiv_user(){
	document.getElementById('user_div').style.display='block';
}
function closeDiv_user(){
	document.getElementById('user_div').style.display='none';
}
</script>