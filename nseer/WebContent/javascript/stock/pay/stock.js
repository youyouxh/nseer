/*
  *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
function registerOk(){
    var table_obj= document.getElementById('bill_body');
    var tr_array=table_obj.rows.length;
    for(var i=1;i<tr_array;i++){
        var amount=document.getElementById('amount'+i).value;
        if(amount!=''){
            var	paid_amount=document.getElementById('paid_amount'+i).value;
            var qualified_amount= document.getElementById('qualified_amount'+i).value;
            if(parseInt(amount)+parseInt(paid_amount)>parseInt(qualified_amount)){
                DWREngine.setAsync(false);
                multiLangValidate.dwrGetLang("erp","本次出库数量加已出库数量不能大于质检合格数量",{
                    callback:function(msg){
                        alert(msg);
                            }
                            });
                        DWREngine.setAsync(true);
                return false;
                }
                }
            }
        var form;
    form =document.getElementById('mutiValidation');
    form.action='../../stock_pay_register_ok';
    if (doValidate('../../xml/stock/stock_pay.xml','mutiValidation')){
    form.submit();
        }
        }
        function checkOk(){
var table_obj= document.getElementById('bill_body');
    var tr_array=table_obj.rows.length;
    for(var i=1;i<tr_array;i++){
        var amount=document.getElementById('amount'+i).value;
            if(amount!=''){
            var	paid_amount=document.getElementById('paid_amount'+i).value;
                var qualified_amount= document.getElementById('qualified_amount'+i).value;
                if(parseInt(amount)+parseInt(paid_amount)>parseInt(qualified_amount)){
                DWREngine.setAsync(false);
                    multiLangValidate.dwrGetLang("erp","本次出库数量加已出库数量不能大于质检合格数量",{
                        callback:function(msg){
                            alert(msg);
                            }
                            });
                        DWREngine.setAsync(true);
                    return false;
                    }
                    }
                    }
                    var form;
            form =document.getElementById('mutiValidation');
    form.action='../../stock_pay_check_ok';
    form.submit();
    }
