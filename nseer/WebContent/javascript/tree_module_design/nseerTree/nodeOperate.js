/* 
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
var bc_tag;
function showAddBrotherDiv(nseer_tree,css_file,xml_file,url){
    var first_xml_file=xml_file.split(',')[0];
    var other_xml_file=xml_file.split(',').length==1?xml_file.split(',')[0]:xml_file.split(',')[1];
    var node=eval(nseer_tree).getSelectNode();
    if(node==null||node.deepID==0){
        return false;
    }
    readXml(css_file,url+other_xml_file);
    bc_tag=0;
    DWREngine.setAsync(false);
    multiLangValidate.dwrGetLang("erp",'添加同级',{
        callback:function(msg){
            document.getElementById('current').innerHTML='<a href="javascript:void(0);"><span align="center">'+msg+'</span></a>';
                }
                });
    DWREngine.setAsync(true);
    }
function showAddChildDiv(nseer_tree,css_file,xml_file,url){
var first_xml_file=xml_file.split(',')[0];
    var other_xml_file=xml_file.split(',').length==1?xml_file.split(',')[0]:xml_file.split(',')[1];
    var node=eval(nseer_tree).getSelectNode();
    if(node==null){
    return false;
        }
    readXml(css_file,url+other_xml_file);
    bc_tag=1;
    }
function showDeleteDiv(nseer_tree,css_file,xml_file,url){
var node=eval(nseer_tree).getSelectNode();
    if(node==null||node.deepID==0){
    return false;
        }
    readXml(css_file,url+xml_file);
    }
function showChangeDiv(nseer_tree,css_file,xml_file,url){
afterChangeDiv(eval(nseer_tree));	
    var first_xml_file=xml_file.split(',')[0];
    var other_xml_file=xml_file.split(',').length==1?xml_file.split(',')[0]:xml_file.split(',')[1];
    var node=eval(nseer_tree).getSelectNode();
    if(node==null||node.deepID==0){
    return false;
        }
    if(node.deepID==1){
        readXml(css_file,url+first_xml_file);
            }else{
    readXml(css_file,url+other_xml_file);
        }
    var table_obj=serverTree(eval(nseer_tree).moduleName,eval(nseer_tree).tableName);
    var field_array=new Array();
    var a=0;
    for(var i=0;i<table_obj.column_array.length;i++){
        var page_id=table_obj.column_array[i].name.toLowerCase();
            var page_obj=document.getElementById(page_id);
            if(page_obj==null||typeof(page_obj)=='undefined'){
            continue;
                }
                field_array[a]=page_id;
            a++;
        }
        var chief_array=[table_obj.column_array[1].name.toLowerCase(),table_obj.column_array[2].name.toLowerCase(),table_obj.column_array[3].name.toLowerCase(),table_obj.column_array[4].name.toLowerCase(),table_obj.column_array[5].name.toLowerCase(),table_obj.column_array[6].name.toLowerCase()];
    DWREngine.setAsync(false);
    NseerModuleTreeDB.getSingleNodeInf(node.name.substring(2),eval(nseer_tree).tableName,field_array,chief_array,{
        callback:function(data_array){
            for(var i=0;i<field_array.length;i++){
                document.getElementById(field_array[i]).value=data_array[i];
                    if(field_array[i]==chief_array[4]){
                        document.getElementById(chief_array[4]+'_hidden').value=data_array[i];
                            }
                        }
                }
            });
    DWREngine.setAsync(true);
    }
function showQueryDiv(nseer_tree,css_file,xml_file,url){
readXml(css_file,url+xml_file);
    }
function addTreeNode(nseer_tree){
beforeAddTreeNode();
{
    var node=eval(nseer_tree).getSelectNode();
        if(node.deepID==0){
            setParentNodeId('-1');
                }else{
        setParentNodeId(eval(nseer_tree).node_list[node.pid].name.substring(2));
            }
        if (!doValidate(eval(nseer_tree).moduleName+'/tree-config.xml',eval(nseer_tree).tableName)) {
            return false;
                }
            var data_array=new Array();
        var field_array=new Array();
        var a=0;
        var table_obj=serverTree(eval(nseer_tree).moduleName,eval(nseer_tree).tableName);
        var file_id=document.getElementById(table_obj.column_array[5].name.toLowerCase()).value;
        var file_name=document.getElementById(table_obj.column_array[6].name.toLowerCase()).value;
        var chief_array=[table_obj.column_array[1].name.toLowerCase(),table_obj.column_array[2].name.toLowerCase(),table_obj.column_array[3].name.toLowerCase(),table_obj.column_array[4].name.toLowerCase(),table_obj.column_array[5].name.toLowerCase(),table_obj.column_array[6].name.toLowerCase()];
        for(var i=0;i<table_obj.column_array.length;i++){
            var page_id=table_obj.column_array[i].name.toLowerCase();
                var page_obj=document.getElementById(page_id);
                if(page_obj==null||typeof(page_obj)=='undefined'){
                continue;
                    }
                    field_array[a]=page_id;
                data_array[a]=page_obj.value;
                a++;
            }
            var parent_node;
        if (bc_tag == 0) {
            parent_node = eval(nseer_tree).node_list[node.pid];
                }else if(bc_tag==1){
            parent_node=node;
                }
            if(parent_node.child_list.length==0&&parent_node.detailsTag==1){
            initTreeNode(parent_node,eval(nseer_tree).moduleName,eval(nseer_tree).tableName);
                }
            doAddNode(eval(nseer_tree).tableName,table_obj.column_array[1].name,parent_node,file_id,file_name,field_array,data_array,chief_array,table_obj.step_length);
        }
    afterAddTreeNode();
    }
function deleteNode(nseer_tree){
beforeDeleteNode();
{
    var table_obj=serverTree(eval(nseer_tree).moduleName,eval(nseer_tree).tableName);
        var chief_array=[table_obj.column_array[1].name.toLowerCase(),table_obj.column_array[2].name.toLowerCase(),table_obj.column_array[3].name.toLowerCase(),table_obj.column_array[4].name.toLowerCase(),table_obj.column_array[5].name.toLowerCase(),table_obj.column_array[6].name.toLowerCase()];
        var node=eval(nseer_tree).getSelectNode();
        if(node!=null&&node.deepID!=0){
            var id1=node.name.substring(2);
                NseerModuleTreeDB.deleteNodeInf(id1,chief_array,eval(nseer_tree).tableName,{
                    callback:function(str){
                        if (str == '200') {
                            node.removeAllChildren();
                                node.remove();
                                }
                            }
                        });
                }
            }
    afterDeleteNode();
    n_D.closeDiv('remove');
    }
function changeNode(nseer_tree){
beforeChangeNode();
{
    var node=eval(nseer_tree).getSelectNode();
        var data_array=new Array();
        var field_array=new Array();
        var a=0;
        var table_obj=serverTree(eval(nseer_tree).moduleName,eval(nseer_tree).tableName);
        var step_length=table_obj.step_length;
        var file_id=document.getElementById(table_obj.column_array[5].name.toLowerCase()).value;
        var file_id_hidden=document.getElementById(table_obj.column_array[5].name.toLowerCase()+'_hidden').value;
        var file_name=document.getElementById(table_obj.column_array[6].name.toLowerCase()).value;
        var chief_array=[table_obj.column_array[1].name.toLowerCase(),table_obj.column_array[2].name.toLowerCase(),table_obj.column_array[3].name.toLowerCase(),table_obj.column_array[4].name.toLowerCase(),table_obj.column_array[5].name.toLowerCase(),table_obj.column_array[6].name.toLowerCase()];
        DWREngine.setAsync(false);
        NseerModuleTreeDB.changeNodeInf(eval(nseer_tree).tableName,node.name.substring(2),file_id,file_id_hidden,file_name,chief_array,step_length,{
            callback:function(parent_node_name){
                if(file_id==file_id_hidden||file_id.substring(0,file_id.length-step_length)==file_id_hidden.substring(0,file_id_hidden.length-step_length)){
                    node.setShowStr(file_id+' '+file_name);
                        }else{
                var node_name=node.name;
                    node.removeAllChildren();
                    node.remove();
                    var parent_node=eval(nseer_tree).getNodeByName('No'+parent_node_name);
                    parent_node.addNode(node_name,file_id+' '+file_name,false,'0',data_array,'eee.jsp');
                    }
                }
                });
        DWREngine.setAsync(true);
        }
    afterChangeNode(eval(nseer_tree));
    n_D.closeDiv('remove');
    }
function quickSearchNode(nseer_tree){
beforeQuickSearchNode();
{
    var table_obj=serverTree(eval(nseer_tree).moduleName,eval(nseer_tree).tableName);
        var field_array=new Array();
        var required_array=new Array();
        var m=0;n=0;
        for(var i=0;i<table_obj.column_array.length;i++){
            if(table_obj.column_array[i].depend=='nodeAttribute'){
                field_array[m]=table_obj.column_array[i].name;
                    m++;
                }
                if(table_obj.column_array[i].depend=='required'){
                required_array[n]=table_obj.column_array[i].name;
                    n++;
                }
                }
            var key_word=document.getElementById('keyword').value;
        if(key_word!=null){
            var rootnode=eval(nseer_tree).getNodeByName("No0");
                rootnode.removeAllChildren();
                rootnode.setShowStr(rootnode.showStr.split("搜索")[0]+" 搜索 \""+key_word+"\"的结果");
                if(rootnode!=null&&rootnode!=""){
                DWREngine.setAsync(false);
                    NseerModuleTreeDB.getNodeInfBySearch(key_word,eval(nseer_tree).tableName,required_array,field_array,{
                        callback:function(result){
                            for(var i=0;i<result.length;i++){
                                var dbCol=result[i].toString();
                                    var nodeInf=dbCol.split('☆');
                                    var nodeName=nodeInf[0].split('◎')[0];
                                    var nodeShowStr=nodeInf[0].split('◎')[1];
                                    var myDetailsTag=nodeInf[0].split('◎')[2];
                                    var myAttributeArray=nodeInf[1].split('◎');
                                    rootnode.addNode("No"+nodeName,nodeShowStr,false,'0',myAttributeArray);
                                    }
                                    }
                                });
                            DWREngine.setAsync(true);
                    }
                    }
                }
            afterQuickSearchNode();
    n_D.closeDiv('remove');
    }
    function doAddNode(table_name,category_id,parent_node,file_id,file_name,field_array,data_array,chief_array,step_length){
if(parent_node!=null){
    DWREngine.setAsync(false);
        NseerModuleTreeDB.getNodeName(table_name,category_id,{
            callback:function(str){
                var nodeName='No'+str;
                    if(nodeName=='No'){
                    DWREngine.setAsync(false);
                        multiLangValidate.dwrGetLang("erp","tree-config.xml文件错误或数据库根节点不存在",{
                            callback:function(msg){
                                alert(msg);
                                }
                                });
                            DWREngine.setAsync(true);
                        return false;
                        }
                        {
                        var picture='';
                        var pic=document.getElementById('nseer_picture').getElementsByTagName('input');
                        for(var i=0;i<pic.length;i++){
                            if(pic[i].type=='radio'){
                                if(pic[i].checked==true){
                                    picture=pic[i].value;
                                        }
                                        }
                                        }	NseerModuleTreeDB.getFileId(table_name,chief_array,parseInt(parent_node.name.substring(2)),step_length,{
                            callback:function(fileId){
                                for(var i=0;i<field_array.length;i++){
                                    if(field_array[i]==chief_array[4]){
                                        data_array[i]=fileId;
                                            }
                                            }
                                            NseerModuleTreeDB.insertNodeInf(table_name,str,parseInt(parent_node.name.substring(2)),fileId+' '+file_name,data_array,field_array,chief_array,document.getElementById('reason').value,document.getElementById('hreflink').value,picture,{
                                    callback:function(str){
                                        parent_node.addNode(nodeName,fileId+' '+file_name,false,'0',data_array,'eee.jsp');
                                            n_D.closeDiv('remove');
                                            }
                                            });
                                        }
                                        });
                                }
                                }
                                });
                        DWREngine.setAsync(true);
        }
        }
        function doubleClick(evt,nseer_tree){
var dbnode=eval(nseer_tree).getSelectNode();	
    if(eval(nseer_tree).getNodeByName("No0").showStr.indexOf("搜索")!=-1){
    }
    else{
    if(dbnode.child_list.length==0&&dbnode.detailsTag==1){
        initTreeNode(dbnode,eval(nseer_tree).moduleName,eval(nseer_tree).tableName);
            }else if(dbnode.child_list.length!=0){
        eval(nseer_tree).openNode(dbnode.id);
            }else{
            afterDoubleClick();
            var url=eval(nseer_tree).moduleName+'/';
            var xml_obj=getXmlObj(eval(nseer_tree).moduleName,eval(nseer_tree).tableName);
            showChangeDiv(xml_obj.getAttribute('tree-name'),xml_obj.parentNode.getAttribute('css'),xml_obj.getAttribute('change-node'),url);
            }
            }
            }
