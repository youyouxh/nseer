/* 
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
function initMyTree(nseer_tree){
    var table_obj=serverTree(nseer_tree.moduleName,nseer_tree.tableName);
    var field_array=new Array();
    var required_array=new Array();
    var m=0;n=0;
    for(var i=0;i<table_obj.column_array.length;i++){
        if(table_obj.column_array[i].depend=='nodeAttribute'){
            field_array[m]=table_obj.column_array[i].name;
            m++;
        }
        if(table_obj.column_array[i].depend=='required'){
            required_array[n]=table_obj.column_array[i].name;
            n++;
        }
    }
    var id=0;
    DWREngine.setAsync(false);
    if(nseer_tree.getNodeByName("No0")!=null&&nseer_tree.getNodeByName("No0")!=""){
        NseerModuleTreeDB.getNodeInf(id,nseer_tree.tableName,required_array,field_array,{
            callback:function(msg){
                var data=msg;
                if(data!=null&&data!=""){
                    var node1=nseer_tree.getNodeByName("No"+id);
                    if(node1!=null&&node1!=""){
                        for(var i=0;i<data.length;i++){
                            var dbCol=data[i].toString();
                            var nodeInf=dbCol.split('☆');
                            var nodeName=nodeInf[0].split('◎')[0];
                            var nodeShowStr=nodeInf[0].split('◎')[1];
                            var myDetailsTag=nodeInf[0].split('◎')[2];
                            var myAttributeArray=nodeInf[1].split('◎');
                            node1.addNode("No"+nodeName,nodeShowStr,false,myDetailsTag,[field_array,myAttributeArray],'eee.jsp');
                            }
                        }
                        }
                    }
                });
        }
        DWREngine.setAsync(true);
    nseer_tree.DrawTree(true);
    nseer_tree.setNodeActiveById(1);
    id=0;
    }
    function initTreeNode(this_node,moduleName,tableName){
var table_obj=serverTree(moduleName,tableName);
    var field_array=new Array();
    var required_array=new Array();
    var m=0;n=0;
    for(var i=0;i<table_obj.column_array.length;i++){
        if(table_obj.column_array[i].depend=='nodeAttribute'){
            field_array[m]=table_obj.column_array[i].name;
                m++;
                }
            if(table_obj.column_array[i].depend=='required'){
            required_array[n]=table_obj.column_array[i].name;
                n++;
                }
            }
            DWREngine.setAsync(false);
    NseerModuleTreeDB.getNodeInf(this_node.name.substring(2),tableName,required_array,field_array,{
        callback:function(msg1){
            if(msg1!=null&&msg1!=''){
                if(this_node!=null&&this_node!=''){
                    for(var i=0;i<msg1.length;i++){
                        var dbCol1=msg1[i].toString();
                            var nodeInf1=dbCol1.split('☆');
                            var nodeName1=nodeInf1[0].split('◎')[0];
                            var nodeShowStr1=nodeInf1[0].split('◎')[1];
                            var myDetailsTag1=nodeInf1[0].split('◎')[2];
                            var myAttributeArray1=nodeInf1[1].split('◎');
                            this_node.addNode('No'+nodeName1,nodeShowStr1,false,myDetailsTag1,[field_array,myAttributeArray1],'eee.jsp');
                            }
                            }
                        }else{
                    }
                }
                });
        DWREngine.setAsync(true);
    }