/* 
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 */
function XmlDocument() {}
XmlDocument.create = function () {
    if (document.implementation && document.implementation.createDocument) {
        return document.implementation.createDocument("", "", null);
    }
}
function readXml(css,url){
    DWREngine.setAsync(false);
    Multi.readXmlToHtml(url,{
        callback:function(html){
            var div_id=html.split('◎')[0];
            if(document.getElementById(div_id)!=null){
                document.getElementById(div_id).style.display='block';
            }else{
                var div1=document.createElement('div');
                div1.id=html.split('◎')[0];
                if(div_id=='dialogDelete'||div_id=='dialogQuery'){
                    div1.innerHTML=html.split('◎')[1]+'</div></div></TD><TD  background="../../images/bg_04.gif"></TD></TR><TR><TD width="1%" height="1%"><IMG  src="../../images/bg_0lbottom.gif" ></TD><TD background="../../images/bg_02.gif"></TD><TD width="1%" height="1%"><IMG  src="../../images/bg_0rbottom.gif"></TD></TR></TBODY></TABLE></div></div-config></div-config>';
                        }else{
                div1.innerHTML=html.split('◎')[1]+'<div id=nseer_picture>'+document.getElementById('nseer_pic').innerHTML+'</div></div></TD><TD  background="../../images/bg_04.gif"></TD></TR><TR><TD width="1%" height="1%"><IMG  src="../../images/bg_0lbottom.gif" ></TD><TD background="../../images/bg_02.gif"></TD><TD width="1%" height="1%"><IMG  src="../../images/bg_0rbottom.gif"></TD></TR></TBODY></TABLE></div></div-config></div-config>';
                    }
                document.body.appendChild(div1);
                }
            }
            });
    DWREngine.setAsync(true);
    }