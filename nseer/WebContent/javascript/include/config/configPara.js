function configPara(){
    this.options;
    this.addInit=function(options){
        var title_del=document.getElementById("title_del");
        if(title_del){
            return false;
        }
        var title_add=document.getElementById("title_add");
        if(title_add){
            return false;
        }
        if(url_temp=='../../../'){
            if(navigator.appName=="Microsoft Internet Explorer"){
                readXml(url_temp+'css/include/nseer_cookie/xml-css.css',url_temp+'xml/include/config/add.xml','0');
            }else{
                readXml(url_temp+'css/include/nseer_cookie/xml-css.css',url_temp+'xml/include/config/firefox_add.xml','0');
            }
        }else{
            if(navigator.appName=="Microsoft Internet Explorer"){
                readXml(url_temp+'css/include/nseer_cookie/xml-css.css',url_temp+'xml/include/add.xml','0');
            }else{
                readXml(url_temp+'css/include/nseer_cookie/xml-css.css',url_temp+'xml/include/firefox_add.xml','0');
            }
        }
        this.options = options || {};
        var title=$("title_add");
        title.innerHTML=this.options.title;
        var table=$("table1");
        for(var i=0;i<this.options.name.length;++i){
            var tr=table.insertRow(table.rows.length);
            var td1=tr.insertCell(0);
            if(options.inputName[i].indexOf("◎")!=-1){
                options.inputName[i]=options.inputName[i].substring(1);
                var input_value="";
                if(options.inputName[i].indexOf("*")!=-1){
                    input_value=options.inputName[i].split("*")[1];
                    options.inputName[i]=options.inputName[i].split("*")[0];
                }
                td1.innerHTML="<textarea name=\""+options.inputName[i]+"\" id=\"inputId_"+i+"\" style=\"width:95%;height:60px;\" value=\""+input_value+"\">";
            }else if(options.inputName[i].indexOf("$")!=-1){
                var op_value=options.inputName[i].substring(0,options.inputName[i].indexOf("$"));
                if(options.inputName[i].substring(0,options.inputName[i].indexOf("$")).indexOf("@")!=-1){
                    op_value=options.inputName[i].substring(0,options.inputName[i].indexOf("$")).split("@")[1];
                }
                td1.innerHTML="<input type=\"hidden\" name=\""+options.inputName[i].substring(options.inputName[i].indexOf("$")+1)+"\" id=\"inputId_"+i+"\" style=\"width:95%;height:60px;\" value=\""+options.inputName[i].substring(0,options.inputName[i].indexOf("$"))+"\">"+op_value;
                options.inputName[i]=options.inputName[i].substring(options.inputName[i].indexOf("$")+1);
            }else if(options.inputName[i].indexOf("⊙")!=-1){
                options.inputName[i]=options.inputName[i].substring(1);
                if(options.inputName[i].indexOf("*")!=-1){
                    input_value=options.inputName[i].split("*")[1];
                    options.inputName[i]=options.inputName[i].split("*")[0];
                }
                if(input_value!=''){
                    if(input_value=='是'){
                        td1.innerHTML="<input name=\""+options.inputName[i]+"\" type=\"radio\" id=\"inputId_"+i+"\" value=\"是\" checked>"+"是"
                        +"<input name=\""+options.inputName[i]+"\" type=\"radio\" id=\"inputId_"+i+"\" value=\"否\">"+"否";
                    }else{
                        td1.innerHTML="<input name=\""+options.inputName[i]+"\" type=\"radio\" id=\"inputId_"+i+"\" value=\"是\">"+"是"
                        +"<input name=\""+options.inputName[i]+"\" type=\"radio\" id=\"inputId_"+i+"\" value=\"否\" checked>"+"否";
                    }
                }else{
                    td1.innerHTML="<input name=\""+options.inputName[i]+"\" type=\"radio\" id=\"inputId_"+i+"\" value=\""+1+"\" checked>"+"是"
                    +"<input name=\""+options.inputName[i]+"\" type=\"radio\" id=\"inputId_"+i+"\" value=\""+0+"\">"+"否";
                }
            }else if(options.inputName[i].indexOf("◇")!=-1){
                var select_name=options.inputName[i].split("◇")[options.inputName[i].split("◇").length-1];
                td1.innerHTML="<SELECT name=\""+select_name+"\" id=\"inputId_"+i+"\" style=\"width:95%\" onChange=\""+options.onChange[i]+"\">"+options.option[i]+"</SELECT>";
                options.inputName[i]=options.inputName[i].split("◇")[options.inputName[i].split("◇").length-1];
            }else if(options.inputName[i].indexOf("^")!=-1){
                options.inputName[i]=options.inputName[i].replace("^","");
                var input_value="";
                if(options.inputName[i].indexOf("*")!=-1){
                    input_value=options.inputName[i].split("*")[1];
                    options.inputName[i]=options.inputName[i].split("*")[0];
                }
                td1.innerHTML="<input type=\"text\" name=\""+options.inputName[i]+"\" id=\"inputId_"+i+"\" style=\"width:95%;\" onkeyup=\"scanfValueValidate(this)\" value=\""+input_value+"\">";
            }else{
                var input_value="";
                if(options.inputName[i].indexOf("*")!=-1){
                    input_value=options.inputName[i].split("*")[1];
                    options.inputName[i]=options.inputName[i].split("*")[0];
                }
                td1.innerHTML="<input type=\"text\" name=\""+options.inputName[i]+"\" id=\"inputId_"+i+"\" style=\"width:95%;\" value=\""+input_value+"\">";
            }
            var td=tr.insertCell(0);
            td.style.width="30%";
            td.style.background='#F1F0EF';
            td.innerHTML=this.options.name[i];
        }
    }
    this.send=function(){
        this.options.send='';
        for(var i=0;i<this.options.name.length;++i){
            if($('inputId_'+i).type=='radio'){
                for(var j=0;j<document.all(this.options.inputName[i]).length;j++){
                    if(document.all(this.options.inputName[i])[j].checked)
                        $('inputId_'+i).value=document.all(this.options.inputName[i])[j].value;
                }
            }
            this.options.send+=this.options.inputName[i]+'='+encodeURI($('inputId_'+i).value)+'&';
        }
        this.options.send=this.options.send.substring(0,this.options.send.length-1);
        if(!doValidate(this.options.xml,'')){
            return false;
        }
        var xmlhttp3;
        if (window.XMLHttpRequest){
            xmlhttp3=new XMLHttpRequest();
        }else if (window.ActiveXObject){
            xmlhttp3=new ActiveXObject("Microsoft.XMLHTTP");
        }if(xmlhttp3){
            xmlhttp3.onreadystatechange = function(){
                if (xmlhttp3.readyState==4){
                    try {
                        if(xmlhttp3.status==200){
                            if(xmlhttp3.responseText=='ok'){
                                window.location.reload();
                            }else{
                                DWREngine.setAsync(false);
                                multiLangValidate.dwrGetLang("erp",xmlhttp3.responseText,{
                                    callback:function(msg){
                                        alert(msg);
                                    }
                                    });
                                DWREngine.setAsync(true);
                            }
                        }else {
                            alert( xmlhttp3.status + '=' + xmlhttp3.statusText);
                        }
                        } catch(exception) {
                        alert(exception);
                    }
                    }
                };
            xmlhttp3.open("POST", this.options.path, true);
            xmlhttp3.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp3.send(this.options.send);
        }
    }
    this.del_reconfirm=function(options){
        var title_del=document.getElementById("title_del");
        if(title_del){
            return false;
        }
        var title_add=document.getElementById("title_add");
        if(title_add){
            return false;
        }
        if(url_temp=='../../../'){
            if(navigator.appName=="Microsoft Internet Explorer"){
                readXml(url_temp+'css/include/nseer_cookie/xml-css.css',url_temp+'xml/include/config/del.xml','0');
            }else{
                readXml(url_temp+'css/include/nseer_cookie/xml-css.css',url_temp+'xml/include/config/firefox_del.xml','0');
            }
        }else{
            if(navigator.appName=="Microsoft Internet Explorer"){
                readXml(url_temp+'css/include/nseer_cookie/xml-css.css',url_temp+'xml/include/del.xml','0');
            }else{
                readXml(url_temp+'css/include/nseer_cookie/xml-css.css',url_temp+'xml/include/firefox_del.xml','0');
            }
        }
        this.options = options || {};
        var title=$("title_del");
        title.innerHTML=this.options.title;
    }
    this.del=function(){
        var ids_str='';
        var tag=0;
        for(var i=1;i<this.options.rows_num;i++){
            if(document.getElementById(i)){
                tag=1;
            }
            if(document.getElementById(i)&&document.getElementById(i).checked){
                ids_str+='⊙'+document.getElementById(i).value;
            }
        }
        if(ids_str==''&&tag==1){
            DWREngine.setAsync(false);
            multiLangValidate.dwrGetLang("erp","请选择要删除的行！",{
                callback:function(msg){
                    alert(msg);
                }
                });
            DWREngine.setAsync(true);
            n_D.closeDiv('remove');
            return false;
        }
        var xmlhttp3;
        if (window.XMLHttpRequest){
            xmlhttp3=new XMLHttpRequest();
        }else if (window.ActiveXObject){
            xmlhttp3=new ActiveXObject("Microsoft.XMLHTTP");
        }if(xmlhttp3){
            xmlhttp3.onreadystatechange = function(){
                if (xmlhttp3.readyState==4){
                    try {
                        if(xmlhttp3.status==200){
                            if(xmlhttp3.responseText=='ok'){
                                DWREngine.setAsync(false);
                                multiLangValidate.dwrGetLang("erp","删除成功",{
                                    callback:function(msg){
                                        alert(msg+'！');
                                    }
                                    });
                                DWREngine.setAsync(true);
                                window.location.reload();
                            }else if(xmlhttp3.responseText.indexOf("/")!=-1){
                                DWREngine.setAsync(false);
                                multiLangValidate.dwrGetLang("erp","正在应用，无法删除，其他均已删除",{
                                    callback:function(msg){
                                        alert(xmlhttp3.responseText+msg+'！');
                                    }
                                    });
                                DWREngine.setAsync(true);
                                window.location.reload();
                            }else{
                                DWREngine.setAsync(false);
                                multiLangValidate.dwrGetLang("erp",xmlhttp3.responseText,{
                                    callback:function(msg){
                                        alert(msg);
                                    }
                                    });
                                DWREngine.setAsync(true);
                                window.location.reload();
                            }
                        }else {
                            alert( xmlhttp3.status + '=' + xmlhttp3.statusText);
                        }
                        } catch(exception) {
                        alert(exception);
                    }
                    }
                };
            xmlhttp3.open("POST", this.options.path, true);
            xmlhttp3.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp3.send('ids_str='+encodeURI(ids_str.substring(1)));
        }
    }
    function $(o){
        return document.getElementById(o);
    }
}