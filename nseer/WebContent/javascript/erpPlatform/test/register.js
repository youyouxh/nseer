function selectChange() {
    document.getElementById("mode_child").value = "";
    if (document.getElementById("search_suggest")) {
        document.getElementById("search_suggest").style.display = "none";
    }
}
if (document.all) {
    document.attachEvent("onmousedown", closeResult);
} else {
    document.addEventListener("mousedown", closeResult, false);
}
var search_div_id = -1;
function search_suggestd_d(obj, event) {
    var e = event || window.event;
    var currentKey = e.charCode || e.keyCode;
    var ss = document.getElementById("search_suggest");
    if (ss) {
        var su = ss.childNodes;
        if (su.length > 0 && (currentKey == 38 || currentKey == 40 || currentKey == 13 || currentKey == 37 || currentKey == 39)) {
            var su = ss.childNodes;
            if (search_div_id >= 0) {
                su[search_div_id].style.backgroundColor = "";
            }
            if (search_div_id >= 0 && currentKey == 13) {
                var temp = su[search_div_id];
                ss.scrollTop = 0;
                search_div_id = -1;
                setSearch(temp.innerHTML, obj);
                return;
            } else {
                if (currentKey == 38 || currentKey == 37) {
                    if (search_div_id <= 0) {
                        search_div_id = 0;
                        ss.scrollTop = 0;
                    } else {
                        search_div_id--;
                    }
                    if (parseInt(su[search_div_id].offsetTop) <= ss.scrollTop) {
                        ss.scrollTop -= ss.scrollTop - parseInt(su[search_div_id].offsetTop);
                    }
                } else {
                    if (currentKey == 40 || currentKey == 39) {
                        if (search_div_id >= su.length - 1) {
                            search_div_id = su.length - 1;
                            ss.scrollTop = ss.scrollHeight;
                        } else {
                            search_div_id++;
                        }
                        if (parseInt(su[search_div_id].offsetTop) + parseInt(su[search_div_id].offsetHeight) - parseInt(ss.style.height) >= ss.scrollTop) {
                            ss.scrollTop += parseInt(su[search_div_id].offsetTop) + parseInt(su[search_div_id].offsetHeight) - ss.scrollTop - parseInt(ss.style.height);
                        }
                    }
                }
            }
            if (search_div_id >= 0) {
                su[search_div_id].style.backgroundColor = "#E8F2FE";
            }
            return;
        }
        search_div_id = -1;
        ss.scrollTop = 0;
    }
}
function search_suggest(obj, event) {
    var e = event || window.event;
    var currentKey = e.charCode || e.keyCode;
    if (currentKey == 38 || currentKey == 40 || currentKey == 13 || currentKey == 37 || currentKey == 39) {
        return;
    }
    var keyword = document.getElementById(obj).value;
    var table_name = document.getElementById("mode_main").value + "_tree";
    var xmlhttp3;
    if (window.XMLHttpRequest) {
        xmlhttp3 = new XMLHttpRequest();
    } else {
        if (window.ActiveXObject) {
            xmlhttp3 = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    if (xmlhttp3) {
        xmlhttp3.onreadystatechange = function () {
            if (xmlhttp3.readyState == 4) {
                try {
                    if (xmlhttp3.status == 200) {
                        if (!document.getElementById("search_suggest")) {
                            var search_suggest = document.createElement("div");
                            search_suggest.id = "search_suggest";
                            search_suggest.style.position = "absolute";
                            search_suggest.style.background = "#FFFFFF";
                            search_suggest.style.overflowY = "auto";
                            search_suggest.style.overflowX = "auto";
                            search_suggest.style.height = "160px";
                            document.body.appendChild(search_suggest);
                        }
                        var cla = document.getElementById("search_suggest");
                        if (xmlhttp3.responseText == "\u2299") {
                            document.getElementById(obj).value = "";
                            document.getElementById("search_suggest").style.display = "none";
                        } else {
                            var options = xmlhttp3.responseText.split("\n");
                            var conter = "";
                            for (var i = 0; i < options.length; i++) {
                                var suggest = "<div style=\"text-align:left\" onmouseover=\"style.backgroundColor='#E8F2FE'\" onmouseout=\"style.backgroundColor=''\"";
                                suggest += "onclick=\"javascript:setSearch('" + options[i] + "','" + obj + "');\" ";
                                suggest += ">" + options[i] + "</div>";
                                conter += suggest;
                            }
                            cla.innerHTML = "";
                            cla.innerHTML = conter;
                            cla.scrollTop = 0;
                            loadDiv1(document.getElementById(obj));
                            cla.style.display = "block";
                        }
                    } else {
                        alert(xmlhttp3.status + "=" + xmlhttp3.statusText);
                    }
                }
                catch (exception) {
                    alert(exception);
                }
            }
        };
        xmlhttp3.open("POST", "register_search.jsp", true);
        xmlhttp3.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp3.send("keyword=" + encodeURI(keyword) + "&table_name=" + table_name);
    }
}
function setSearch(obj, obj1) {
    var value = obj;
    document.getElementById(obj1).value = value;
    document.getElementById("search_suggest").style.display = "none";
}
function closeResult(e) {
    e = window.event || e;
    var scrollTop = parseInt(getScrollTop());
    var port_X = parseInt(e.clientX || e.pageX);
    var port_Y = parseInt(e.clientY || e.pageY) + scrollTop;
    if (document.getElementById("search_suggest") && document.getElementById("search_suggest").style.display == "block") {
        var div = document.getElementById("search_suggest");
        var div_w = parseInt(div.style.width);
        var div_h = parseInt(getRStyle(div, "height", "height"));
        var div_l = parseInt(div.style.left);
        var div_t = parseInt(div.style.top);
        if (port_X < div_l || port_X > (div_l + div_w) || port_Y < div_t || port_Y > (div_t + div_h)) {
            if (div.style.display == "block") {
                if (document.getElementById("file_name")) {
                    document.getElementById("file_name").value = "";
                }
            }
            div.innerHTML = "";
            div.style.display = "none";
        }
    }
}
function getScrollTop() {
    var scrollTop = 0;
    if (document.documentElement && document.documentElement.scrollTop) {
        scrollTop = document.documentElement.scrollTop;
    } else {
        if (document.body) {
            scrollTop = document.body.scrollTop;
        }
    }
    return scrollTop;
}
function getRStyle(elem, IE, FF) {
    return navigator.appName == "Microsoft Internet Explorer" ? elem.currentStyle[IE] : document.defaultView.getComputedStyle(elem, "").getPropertyValue(FF);
}
function relieving() {
    document.detachEvent("onmousedown", closeResult);
}
function loadDiv1(obj1) {
    var w = obj1.offsetWidth;
    var h = obj1.offsetHeight - 2;
    var x = obj1.offsetLeft, y = obj1.offsetTop;
    while (obj1 = obj1.offsetParent) {
        x += obj1.offsetLeft;
        y += obj1.offsetTop;
    }
    var obj = document.getElementById("search_suggest");
    obj.style.width = w;
    obj.style.height = "100px";
    obj.style.background = "yellow";
    obj.style.position = "absolute";
    obj.style.top = y + h;
    obj.style.left = x;
}
function submitForm(formId) {
    var cn_name = document.getElementById("mode_main_cn");
    var sel = document.getElementById("mode_main");
    for (var i = 0; i < (sel.options).length; ++i) {
        if ((sel.options[i]).selected) {
            cn_name.value = (sel.options[i]).innerHTML;
            break;
        }
    }
    if(doValidate('../../xml/document/document_maintest.xml','mainRegister'))
        document.getElementById(formId).submit();
}
