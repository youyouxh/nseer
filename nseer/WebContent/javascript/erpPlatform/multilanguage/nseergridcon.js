function del(num){
    var id_group='';
    for(var i=1;i<num;i++){
        if(document.getElementById(i).checked) id_group+=document.getElementById(i).value+'⊙';
    }
    id_group=id_group.substring(0,id_group.length-1);
    var xmlhttp3;
    if (window.XMLHttpRequest){
        xmlhttp3=new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp3=new ActiveXObject("Microsoft.XMLHTTP");
    }if(xmlhttp3) {
        xmlhttp3.onreadystatechange = function() {
            if (xmlhttp3.readyState==4){
                try {
                    if(xmlhttp3.status==200) {
                        if(parseInt(xmlhttp3.responseText)==1){
                            DWREngine.setAsync(false);
                            multiLangValidate.dwrGetLang("erp","请选择要删除的行！",{
                                callback:function(msg){
                                    alert(msg);
                                }
                            });
                            DWREngine.setAsync(true);
                        }
                        document.getElementById('reconfirm').style.display='none';
                        window.location.reload();
                    }else{
                        alert( xmlhttp3.status + '=' + xmlhttp3.statusText);
                    }
                } catch(exception) {
                    alert(exception);
                }
            }
        };
        xmlhttp3.open("POST", "../../erpPlatform_multilanguage_batch_delete_ok", true);
        xmlhttp3.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp3.send('id_group='+id_group);
    }
}
var all='';
function sigle_del(id){
    all=id;
    document.getElementById('reconfirm1').style.display='block';
}
function del1(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp=new XMLHttpRequest();
    }else if
    (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }if(xmlhttp)

    {
        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState==4){
                try {
                    if(xmlhttp.status==200){
                        document.getElementById('reconfirm1').style.display='none';
                        window.location.reload();
                    }else {
                        alert( xmlhttp.status + '=' + xmlhttp.statusText);
                    }
                } catch(exception) {
                    alert(exception);
                }
            }
        };
        xmlhttp.open("POST", "../../erpPlatform_multilanguage_delete_ok", true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.send('id='+all);
    }
}
var star=false;
var flagX;
var flagY;
var inputArray = new Array();
var tempArray = new Array();
var iX;
var iY;
var iW;
var iH;
var lastSelect;
var firstSelect;
var delRowName = "";
var delSelectName="";
var copySelectName="";
var pasteSelectName="";
DWREngine.setAsync(false);
multiLangValidate.dwrGetLang("erp",'清空列',{
    callback:function(msg){
        delRowName=msg;
    }
});
multiLangValidate.dwrGetLang("erp",'清空选择项',{
    callback:function(msg){
        delSelectName=msg;
    }
});
multiLangValidate.dwrGetLang("erp",'复制选择项',{
    callback:function(msg){
        copySelectName=msg;
    }
});
multiLangValidate.dwrGetLang("erp",'粘贴选择项',{
    callback:function(msg){
        pasteSelectName=msg;
    }
});
DWREngine.setAsync(true);
var dataobj=nseer_grid.$(nseer_grid.callname + "_datacolumn");
function $empty(){};
function $false(){
    return false;
};
function inputStar(obj,e){
    e = window.event || e;
    if(e.button==2 && obj.getAttribute("select")==1){
        clsSelectInput(e);return;
    };
    if(e.button==2 && tempArray.length>0){
        inputPaste(obj,e);return;
    };
    flagX = e.clientX || e.pageX;
    flagY = e.clientY || e.pageY;
    iX = obj.parentNode.offsetLeft;
    iY = obj.parentNode.offsetTop;
    iW = obj.parentNode.offsetWidth;
    iH = obj.parentNode.offsetHeight;
    firstSelect=obj;
    star=true;
    for(var i in inputArray){
        inputArray[i].style.background='';
        inputArray[i].setAttribute("select",0);
    }
    inputArray.length=0;
}
function addEvent(oElement,sEvent,func){ 	
    if (oElement.attachEvent){
        oElement.attachEvent(sEvent,func);
    }else{
        sEvent=sEvent.substring(2,sEvent.length);
        oElement.addEventListener(sEvent,func,false);
    }
}
function inputOnFocus(o){
    o.style.background="#F9A409"
    nseer_grid.inputSelect=1;
}
function inputOnBlur(o){
    if(!o.getAttribute("select") || o.getAttribute("select")!=1){
        o.style.background='';
        nseer_grid.inputSelect=0;
    }
}
function inputOnKeyDown(o,e){
    e = e || window.event;
    var currentKey = e.charCode||e.keyCode;
    if(currentKey==38 || currentKey==40 || currentKey==37 || currentKey==39){
        if(inputArray.length>0){
            for(var x in inputArray){
                inputArray[x].style.background='';
                inputArray[x].setAttribute("select",0);
            }
            inputArray.length=0;
        }
        var cellIndex=o.parentNode.cellIndex;
        var rowIndex = o.parentNode.parentNode.rowIndex;
        var tds = o.parentNode.parentNode.cells;
        var i;
        if(currentKey==37){
            if(cellIndex-1<0)return;
            i = tds[cellIndex-1];
        }else if(currentKey==38){
            if(rowIndex<2)return;
            i = dataobj.childNodes[0].rows[rowIndex-1].cells[cellIndex];
        }else if(currentKey==39){
            if(!tds[cellIndex+1])return;
            i = tds[cellIndex+1];
        }else{
            if(!dataobj.childNodes[0].rows[rowIndex+1])return;
            i = dataobj.childNodes[0].rows[rowIndex+1].cells[cellIndex];
        }
        if(i.childNodes[0].tagName && i.childNodes[0].tagName.toLowerCase()=='input'){
            nseer_grid.updown(currentKey,o.parentNode);
            i.childNodes[0].focus();
        }
    }
}
addEvent(document,"onmousemove",selectMove);
addEvent(document,"onmouseup",function(){
    star=false;
});
function selectMove(e){
    if(star==false)return;	
    e = window.event || e;	
    x = e.clientX || e.pageX;
    y = e.clientY || e.pageY;
    if(x>=flagX && y>=flagY){
        selectInput(x-flagX,y-flagY);
    }
    movestar=true;
}
var input=document.getElementsByTagName('input');		
function selectInput(xlen,ylen){ 
    clearArray();
    for(var i=0;i<input.length;++i){
        if(input[i].className.indexOf('excel_input')!=-1){
            var oT = input[i].parentNode.offsetTop || 0;
            if(oT>iY+ylen)break;
            var oW = input[i].parentNode.offsetWidth || 0;
            var oH = input[i].parentNode.offsetHeight || 0;
            var oL = input[i].parentNode.offsetLeft || 0;
            if(oL>=iX&&oT>=iY && oL<=iX+xlen&&oT<=iY+ylen){
                input[i].style.background='#F9A409';
                input[i].setAttribute("select",1);
                inputArray.push(input[i]);
            }else{
                input[i].setAttribute("select",0);
                input[i].style.background='';
            }
        }
    }
}
function clearArray(){
    for(var i in inputArray){
        inputArray[i].style.background='';
    }
    inputArray.length=0;
}
document.onkeydown=function(e){
    e = window.event || e;
    var currentKey = e.keyCode || e.keyChar;
    if(currentKey==46){
        for(var i in inputArray){
            inputArray[i].value='';
            inputArray[i].style.background='';
            inputArray[i].setAttribute("select",0);
        }
        inputArray.length=0;
    }
}
nseer_grid.rsc_d_2=function(e,obj){
    document.oncontextmenu=$false;
    e = e || window.event;
    var x=e.clientX||e.pageX;
    var y=e.clientY||e.pageY;
    var index = obj.cellIndex;
    var drag_div = nseer_grid.$("drag_div2");
    drag_div.style.left=x+'px';
    drag_div.style.top=y+'px';
    drag_div.style.zIndex = 10;
    drag_div.style.display ='';
    drag_div.innerHTML = "<div style=\"padding:3px 0px 0px 0px\">&nbsp;&nbsp;" + delRowName + "</div>";
    drag_div.onclick=function(){
        for (var i = 1; i < dataobj.childNodes[0].rows.length; i++) {
            var input = dataobj.childNodes[0].rows[i].cells[index].childNodes[0];
            if(input.tagName && input.tagName.toLowerCase()=='input'){
                input.value='';
            }
        }
        document.oncontextmenu=$empty;
        this.style.display='none';
    }
    drag_div.onmouseout=function(){
        this.style.display='none';
        document.oncontextmenu=$empty;
    }
    drag_div.onmouseover=function(){
        this.style.display='';
    }
}
function clsSelectInput(e){
    document.oncontextmenu=$false;
    e = e || window.event;
    var x=e.clientX||e.pageX;
    var y=e.clientY||e.pageY;
    var menu = nseer_grid.$("sel_menu");
    var cls_div = nseer_grid.$("drag_div3");
    var copy_div = nseer_grid.$("drag_div4");
    menu.style.position='absolute';
    menu.style.left=x+'px';
    menu.style.top=y+'px';
    menu.style.zIndex = 10;
    menu.style.display ='';
    cls_div.innerHTML = "<div style=\"padding:3px 0px 0px 0px;margin:3px;\">&nbsp;&nbsp;" + delSelectName + "</div>";
    copy_div.innerHTML = "<div style=\"padding:3px 0px 0px 0px;margin:3px;\">&nbsp;&nbsp;" + copySelectName + "</div>";
    cls_div.onclick=function(){
        for (var i = 0; i < inputArray.length; i++) {
            inputArray[i].value='';
            inputArray[i].style.background='';
            inputArray[i].setAttribute("select",0);
        }
        document.oncontextmenu=$empty;
        menu.style.display='none';
    }
    menu.onmouseout=function(){
        this.style.display='none';
        document.oncontextmenu=$empty;
    }
    menu.onmouseover=function(){
        this.style.display='';
    }
    cls_div.onmouseout=function(){
        menu.style.display='none';
        this.style.background='';
        document.oncontextmenu=$empty;
    }
    cls_div.onmouseover=function(){
        menu.style.display='';
        this.style.background='#fff';
    }
    copy_div.onmouseout=function(){
        menu.style.display='none';
        this.style.background='';
        document.oncontextmenu=$empty;
    }
    copy_div.onmouseover=function(){
        this.style.background='#fff';
        menu.style.display='';
    }
    copy_div.onclick=function(){
        menu.style.display='none';
        copySelect();
    }
}
function copySelect(){
    tempArray.length=0;
    for(var i in inputArray){
        inputArray[i].style.background='';
        inputArray[i].setAttribute("select",0);
        tempArray[i]=inputArray[i];
    }
    inputArray.length=0;
}
function inputPaste(o,e){
    document.oncontextmenu=$false;
    var x=e.clientX||e.pageX;
    var y=e.clientY||e.pageY;
    var paste_div = nseer_grid.$("drag_div5");
    paste_div.style.left=x+'px';
    paste_div.style.top=y+'px';
    paste_div.style.zIndex = 10;
    paste_div.style.display ='';
    paste_div.innerHTML = "<div style=\"padding:3px 0px 0px 0px;margin:3px;\">&nbsp;&nbsp;" + pasteSelectName + "</div>";
    paste_div.onmouseout=function(){
        this.style.display='none';
        document.oncontextmenu=$empty;
    }
    paste_div.onmouseover=function(){
        this.style.display='';
    }
    paste_div.onclick=function(){
        document.oncontextmenu=$empty;
        this.style.display='none';
        pasteSelect(o);
    };
}
function pasteSelect(o){
    document.oncontextmenu=$empty;
    if(tempArray.length<1)return;
    var cellIndex0=tempArray[0].parentNode.cellIndex;
    var rowIndex0=tempArray[0].parentNode.parentNode.rowIndex;
    var cellIndex=o.parentNode.cellIndex;
    var rowIndex = o.parentNode.parentNode.rowIndex;
    for(var i in tempArray){
        var icellIndex = tempArray[i].parentNode.cellIndex - cellIndex0;
        var irowIndex = tempArray[i].parentNode.parentNode.rowIndex - rowIndex0;
        if(dataobj.childNodes[0].rows[rowIndex+irowIndex] && dataobj.childNodes[0].rows[rowIndex+irowIndex].cells[cellIndex+icellIndex] && dataobj.childNodes[0].rows[rowIndex+irowIndex].cells[cellIndex+icellIndex].childNodes[0].tagName.toLowerCase()=='input'){
            dataobj.childNodes[0].rows[rowIndex+irowIndex].cells[cellIndex+icellIndex].childNodes[0].value=tempArray[i].value;
        }
    }
}