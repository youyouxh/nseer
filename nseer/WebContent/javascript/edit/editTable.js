function setInputValue(objRow,strName,strValue){
    var objs=objRow.all;
    for(var i=0;i<objs.length;i++){
        if(objs[i].name==strName){
            objs[i].value=strValue;
        }
        if(strName=='product_describe1'&&objs[i].name=='product_describe_ok'){
            objs[i].innerHTML=strValue;
        }
    }
    }
var k=0;
function addInstanceRow(objTable,Names,Values){
    var tbodyOnlineEdit=objTable.getElementsByTagName("TBODY")[0];
    var theadOnlineEdit=objTable.getElementsByTagName("THEAD")[0];
    var elm = theadOnlineEdit.lastChild.cloneNode(true)
    elm.style.display="";
    for(var i=0;i<Names.length;i++){
        setInputValue(elm,Names[i],Values[i]);
        tbodyOnlineEdit.insertBefore(elm);
    }
    k++;
}
function setOnlineEditCheckBox(obj,value){
    var tbodyOnlineEdit=obj.getElementsByTagName("TBODY")[0];
    for (var i=tbodyOnlineEdit.children.length-1; i>=0 ; i-- )
        tbodyOnlineEdit.children[i].firstChild.firstChild.checked=value;
}
function deleteRow(objTable){
    var tbodyOnlineEdit=objTable.getElementsByTagName("TBODY")[0];
    for (var i=tbodyOnlineEdit.children.length-1; i>=0 ; i-- )
        if (tbodyOnlineEdit.children[i].firstChild.firstChild.checked)
            tbodyOnlineEdit.deleteRow(i);
}
function checkRow(objTable,unique) {
    var tbodyOnlineEdit=objTable.getElementsByTagName("TBODY")[0];
    for (var i=tbodyOnlineEdit.children.length-1; i>=0 ; i-- ){
        var temp=tbodyOnlineEdit.children[i].childNodes[1].childNodes[0].value;
        if(unique==temp) return false;
    }
    return true;
}
