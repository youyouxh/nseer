/*
 * CodePress regular expressions for Text syntax highlighting
 */
Language.syntax = [];
Language.snippets = [];
Language.complete = [];
Language.shortcuts = [];