<!--
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 -->
<%@page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*,include.nseer_cookie.*" import="java.util.*" import="java.io.*" import="include.nseer_db.*,java.text.*"%>
<%@page import="include.anti_repeat_submit.Globals"%>
<%@include file="../include/head.jsp"%>
<jsp:useBean id="xml" class="include.nseer_cookie.ReadXmlLength" scope="page"/>
<script language=javascript src="../../javascript/winopen/winopen.js"></script>
<jsp:useBean id="demo" class="include.tree_index.businessComment" scope="page"/>
<%
DealWithString DealWithString=new DealWithString(application);
String mod=request.getRequestURI();
demo.setPath(request);
String handbook=demo.businessComment(mod,"您正在做的业务是：","document_main","reason","value");
%>
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2">
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_HANDBOOK_STYLE1%> class="TD_HANDBOOK_STYLE1"><div class="div_handbook"><%=handbook%></div></td>
</tr>
</table>
<%
try{
String table=request.getParameter("table");
MaxKind max=new MaxKind((String)session.getAttribute("unit_db_name"),"design_config_file_kind","file_id");
ServletContext context=session.getServletContext();
String path=context.getRealPath("/");
if(table.equals("crm_file")){
max=new MaxKind((String)session.getAttribute("unit_db_name"),"crm_config_file_kind","file_id");
path=path+"xml/crm/config/file/tree-config.xml";
}else{
path=path+"xml/design/config/file/tree-config.xml";
}
int first_length=Integer.parseInt(xml.read(path,"table","first-length"));
int step_length=Integer.parseInt(xml.read(path,"table","step-length"));
int kind_rows=KindDeep.getDeep(max.maxValue("file_id"),first_length,step_length);
max.close();
String table_id=request.getParameter("table_id");
String table_name=request.getParameter("table_name");
String intrmessage_ID=request.getParameter("intrmessage_ID");
String checker_ID=(String)session.getAttribute("human_IDD");
String checker=(String)session.getAttribute("realeditorc");
String choose_value=request.getParameter("type");
java.util.Date now=new java.util.Date();
SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
String time=formatter.format(now);
nseer_db db=new nseer_db((String)session.getAttribute("unit_db_name"));
nseer_db db1=new nseer_db((String)session.getAttribute("unit_db_name"));
%>
<FORM METHOD="POST" id="mutiValidation" name="selectList" ACTION="../../oa_intrmessage_check_type_ok?<%=Globals.TOKEN_KEY%>=<%=session.getAttribute(Globals.TOKEN_KEY)%>">
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2">
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE3%> class="TD_STYLE3"><div <%=DIV_STYLE1%> class="DIV_STYLE1"><%String sql1="select * from oa_config_public_char where kind='群发工具分类' order by type_ID";
ResultSet rs1=db.executeQuery(sql1);
while(rs1.next()){if(!rs1.getString("type_name").equals("QQ")&&!rs1.getString("type_name").equals("msn")){%><INPUT name="check_type" type="checkbox" value="<%=exchange.toHtml(rs1.getString("type_name"))%>" nseer=""><%=exchange.toHtml(rs1.getString("type_name"))%>&nbsp;<%}}%><input type="button" <%=BUTTON_STYLE1%> class="BUTTON_STYLE1" value="<%=demo.getLang("erp","全选")%>" id="check" name="check" onClick=selAl(this.form)>&nbsp;<input type="submit" <%=SUBMIT_STYLE1%> class="SUBMIT_STYLE1" value="<%=demo.getLang("erp","提交")%>" name="B1">&nbsp;<input type="button" <%=BUTTON_STYLE1%> class="BUTTON_STYLE1" value="<%=demo.getLang("erp","返回")%>" onClick="history.back()"></div>
</td>
</tr>
</table>
<%
String first_kind="";
String second_kind="";
String third_kind="";
int n=0;
String sql="select * from "+table+" where check_tag='1' order by chain_id,"+table_id;
ResultSet rs=db.executeQuery(sql);
%>
<div id="send_msg_checkbox"><!-- 设置全选的层 -->
<div id="nseer_grid_div"></div>
<script type="text/javascript">
function id_link(link){
document.location.href=link;
}
var nseer_grid=new nseergrid();
nseer_grid.callname="nseer_grid";
nseer_grid.parentNode=nseer_grid.$("nseer_grid_div");
nseer_grid.columns=[
<%
for(int i=1;i<=kind_rows;i++){
if(table.equals("crm_file")){
%>
{name: '<%=i%><%=demo.getLang("erp","级客户分类")%>'},
<%}else{%>
{name: '<%=i%><%=demo.getLang("erp","级产品分类")%>'},
<%}}%>    
{name: '<%=demo.getLang("erp","名称")%>'}
]
nseer_grid.column_width=[
<%for(int i=1;i<=kind_rows;i++){%>90,<%}%>
190];
nseer_grid.auto='<%=demo.getLang("erp","名称")%>';
nseer_grid.data=[
<%
String sq1="";
if(table.equals("crm_file")){
sq1="select * from  crm_config_file_kind where PARENT_CATEGORY_ID!='-1' order by file_id";
}else{
sq1="select * from  design_config_file_kind where PARENT_CATEGORY_ID!='-1' order by file_id";
}
rs=db.executeQuery(sq1);
int row_tag=0;
while(rs.next()){
String file_id=rs.getString("file_id").trim();
if(!file_id.equals("")){
int len=KindDeep.getDeep(file_id,first_length,step_length);
String sq11="select * from  "+table+" where chain_id='"+rs.getString("file_id")+"' and check_tag='1' order by chain_id";
rs1=db1.executeQuery(sq11);
String chain_name_temp=rs.getString("chain_name");
String chain_name_temp1=rs.getString("chain_name");
if(chain_name_temp.indexOf("-")!=-1)chain_name_temp=chain_name_temp.substring(chain_name_temp.lastIndexOf("-")+1,chain_name_temp.length());
%>
[
<%
String src="";
if(table.equals("crm_file")){
src="../../"+table.split("_")[0]+"/file/query.jsp?customer_ID=";
}else{
src="../../"+table.split("_")[0]+"/file/query.jsp?provider_ID=";
}
String cont="'<div><input type=\"checkbox\"  nseer="+rs.getString("file_id")+"  onclick=\"select_value(this)\">"+chain_name_temp+"</div>',";
for(int i=0;i<len;i++){
if(i<len-1&&row_tag==0){%>
'<%=chain_name_temp1.split("-")[i]%>',
<%}else if(i!=len-1){%>'',<%}else{%><%=cont%>
<%
}}
for(int j=0;j<kind_rows-len;j++){%>'',<%}
List major_data=(List)new java.util.ArrayList();
while(rs1.next()){
major_data.add(rs1.getString("chain_id")+"◎"+rs1.getString(table_id)+"◎"+rs1.getString(table_name));
}
%>
'<%for(Iterator it=major_data.iterator();it.hasNext();){String name=(String) it.next();String name0=name.split("◎")[0];String name1=name.split("◎")[1];String name2=name.split("◎")[2];if(name0.equals(rs.getString("file_id"))){%><div><input type="checkbox" nseer="<%=name0%>1" name=\"chain_id\" value="<%=name1%>/<%=exchange.toHtml(name2)%>" >&nbsp;<a href="<%=src%><%=name1%>"><%=exchange.toHtml(name2)%>/<%if(name1.indexOf("@")!=-1){%><%=name1.split("@")[1]%><%}else{%><%=name1%><%}%></a></div><%}}%>'],<%row_tag++;}}%>
['']];
nseer_grid.init();
</script>
<div id="drag_div"></div>
<div id="point_div_t"></div>
<div id="point_div_b"></div>
<input type="hidden" name="choose_value" value="<%=exchange.toHtml(choose_value)%>">
<input type="hidden" name="table_id" value="<%=table_id%>">
<input type="hidden" name="table_name" value="<%=exchange.toHtml(table_name)%>">
<input type="hidden" name="intrmessage_ID" value="<%=intrmessage_ID%>">
<input type="hidden" name="cols_number" value="<%=n%>">
<input type="hidden" name="checker" value="<%=exchange.toHtml(checker)%>">
<input type="hidden" name="checker_ID" value="<%=checker_ID%>">
<input type="hidden" name="check_time" value="<%=exchange.toHtml(time)%>">
<input type="hidden" name="table" value="<%=table%>">
<input type="hidden" name="table_id" value="<%=table_id%>">
<input type="hidden" name="table_name" value="<%=table_name%>">
<script>
function select_value(s){
var file_id=s.getAttribute('nseer').split('/')[0];
var value_length=file_id.length;
if(s.checked){
var checkbox_array=document.getElementsByTagName('input');
for(var i=0;i<checkbox_array.length;i++){
if(checkbox_array[i].type=='checkbox'&&checkbox_array[i].getAttribute('nseer').substring(0,value_length)==file_id&&checkbox_array[i].getAttribute('nseer').length>value_length){
checkbox_array[i].checked=true;
}}
}else{
var checkbox_array=document.getElementsByTagName('input');
for(var i=0;i<checkbox_array.length;i++){
if(checkbox_array[i].type=='checkbox'&&checkbox_array[i].getAttribute('nseer').substring(0,value_length)==file_id&&checkbox_array[i].getAttribute('nseer').length>value_length){
checkbox_array[i].checked=false;
}}}}
function selAl(){
var checkbox_array=document.getElementById('send_msg_checkbox').getElementsByTagName('input');
if(document.getElementById('check').value=='反选'){
for(var i=0;i<checkbox_array.length;i++){
if(checkbox_array[i].type=='checkbox'){
checkbox_array[i].checked=false;
}}
document.getElementById('check').value='全选';
}else{
for(var i=0;i<checkbox_array.length;i++){
if(checkbox_array[i].type=='checkbox'){
checkbox_array[i].checked=true;
}}
document.getElementById('check').value='反选';
}}
</script>
<input type="hidden" name="<%=Globals.TOKEN_KEY%>" value="<%=session.getAttribute(Globals.TOKEN_KEY)%>">
</form>
<%
db.close();
db1.close();
}catch(Exception ex){ex.printStackTrace();}
%>