<!--
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 -->
<%@page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*,include.nseer_cookie.*" import="java.util.*" import="java.io.*" import="include.nseer_cookie.exchange" import="include.nseer_db.*,java.text.*"%>
<%nseer_db oa_db=new nseer_db((String)session.getAttribute("unit_db_name"));%>
<%@page import="include.anti_repeat_submit.Globals"%>
<%@taglib uri="/WEB-INF/mytag.tld" prefix="page"%>
<jsp:useBean id="validata" scope="page" class="validata.ValidataNumber"/>
<jsp:useBean id="available" class="stock.getBalanceAmount" scope="request"/>
<jsp:useBean id="column" class="include.get_sql.getKeyColumn" scope="page"/>
<%@include file="../include/head_list.jsp"%>
<jsp:useBean id="NseerSql" class="include.query.NseerSql" scope="page"/>
<jsp:useBean id="mask" class="include.operateXML.Reading"/>
<jsp:setProperty name="mask" property="file" value="xml/oa/oa_messager.xml"/>
<script type="text/javascript" src="../../javascript/qcs/config/publics/dealwith.js"></script>
<jsp:useBean id="demo" class="include.tree_index.businessComment" scope="page"/>
<%
DealWithString DealWithString=new DealWithString(application);
String mod=request.getRequestURI();
demo.setPath(request);
String handbook=demo.businessComment(mod,"您正在做的业务是：","document_main","reason","value");
%>
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2">
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_HANDBOOK_STYLE1%> class="TD_HANDBOOK_STYLE1"><div class="div_handbook"><%=handbook%></div></td>
</tr>
</table>
<form method="post" id="mutiValidation" name="keyRegister" action="">
<%
String type=request.getParameter("type");
String intrmessage_ID=request.getParameter("intrmessage_ID");  
String checker_id=(String)session.getAttribute("human_IDD");
String checker=(String)session.getAttribute("realeditorc");
java.util.Date now=new java.util.Date();
SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
String check_time=formatter.format(now);
String tablename="oa_messager";
String condition="";
String fileName="check.jsp";
String validationXml="../../xml/oa/oa_messager.xml";
String nickName="群发对象";
String queue="order by register_time desc";
if(type!=null){
condition="type='"+type+"'";
}
%>
<%@include file="../../include/search.jsp"%>
<%
try{
ResultSet rs=oa_db.executeQuery(sql_search);
otherButtons="&nbsp;<input type=\"button\" "+BUTTON_STYLE1+" class=\"BUTTON_STYLE1\" id=\"select_all\" value=\""+demo.getLang("erp","全选")+"\" name=\"check\" onclick=\"selAll()\">"+"&nbsp;<input type=\"button\" "+BUTTON_STYLE1+" class=\"BUTTON_STYLE1\" id=\"B1\" value=\""+demo.getLang("erp","提交")+"\" name=\"B1\" onclick=\"clicktosubmit()\" >"+"&nbsp;<input type=\"button\" "+BUTTON_STYLE1+" class=\"BUTTON_STYLE1\" id=\"resert\" value=\""+demo.getLang("erp","返回")+"\" name=\"resert\" onclick=\"history.back()\">";
int k=1;
%>
<input type="hidden" id="intrmessage_ID" name="intrmessage_ID" value="<%=intrmessage_ID%>">
<input type="hidden" id="checker" name="checker" value="<%=checker%>">
<input type="hidden" id="checker_id" name="checker_id" value="<%=checker_id%>">
<input type="hidden" id="check_time" name="check_time" value="<%=check_time%>">
<%@include file="search_top_intr.jsp"%>
<div id="nseer_grid_div"></div>
<script type="text/javascript">
function id_link(link){
document.location.href=link;
}
var nseer_grid=new nseergrid();
nseer_grid.callname="nseer_grid";
nseer_grid.parentNode=nseer_grid.$("nseer_grid_div");
nseer_grid.columns=[
{name: '&nbsp;'},
{name: '<%=demo.getLang("erp","群发工具类型")%>'},
{name: '<%=demo.getLang("erp","内容")%>'}
]
nseer_grid.column_width=[40,200,300];
nseer_grid.auto='<%=demo.getLang("erp","内容")%>';
nseer_grid.data=[
<page:pages rs="<%=rs%>" strPage="<%=strPage%>">
['<input type="checkbox" id="draft_gar<%=k%>" name="row_id" value="<%=rs.getString("id")%>" style="height:10">','<%=rs.getString("tool_type")%>','<%=rs.getString("content")%>'],
</page:pages>
['']];
nseer_grid.init();
</script>
<div id="drag_div"></div>
<div id="point_div_t"></div>
<div id="point_div_b"></div>
<%@include file="../../include/search_bottom.jsp"%>
<page:updowntag_intr num="<%=intRowCount%>" file="<%=fileName%>"  type="<%=type %>" intrmessage_ID="<%=intrmessage_ID %>"/>
<%oa_db.close();}catch(Exception ex){ex.printStackTrace();}%>
</FORM>
<script>
resultFormSubmit1=function (url,inputTextId1,inputTextId2,validationXml,sub_tag,type,intrmessage_ID){
var keyword=document.getElementById(inputTextId1).value;
var u=window.location.href.split('://')[1].split('?')[0].split('/');
var url1='';
for(var i=0;i<u.length-3;i++){url1+='../';}
var xmlhttp;
if(window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}else if(window.ActiveXObject){xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}if(xmlhttp){xmlhttp.onreadystatechange=function(){
if(xmlhttp.readyState==4){try{if(xmlhttp.status==200){
}else{alert( xmlhttp.status + '=' + xmlhttp.statusText);}}catch(exception){alert(exception);}}};
xmlhttp.open("POST", url1+"include/search_ajax.jsp", false);
xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
xmlhttp.send('search_tag=1&&keyword='+encodeURI(keyword));
}
if(sub_tag=='0'){
document.getElementById(inputTextId2).value=keyword;
}else if(sub_tag=='1'){
document.getElementById(inputTextId2).value=document.getElementById(inputTextId2).value+'⊙'+keyword;
}else if(sub_tag=='2'){
}
keyword=document.getElementById(inputTextId2).value;
window.location.href=url+'?readXml=n&'+inputTextId2+'='+encodeURI(keyword)+'&type='+type+'&intrmessage_ID='+intrmessage_ID;
}
</script>
<script>
function clicktosubmit(){
var id_value='-';
var form=document.getElementById('mutiValidation');
var intrmessage_ID=document.getElementById('intrmessage_ID').value;
var checkbox_array=document.getElementsByTagName('input');
for(i=0;i<checkbox_array.length;i++){
if(checkbox_array[i].type=='checkbox'&&checkbox_array[i].checked==true){
id_value=id_value+'@'+checkbox_array[i].value;
}}
form.action='../../oa_intrmessage_check_ok?intrmessage_ID='+intrmessage_ID+'&id_value='+id_value+'<%=Globals.TOKEN_KEY%>=<%=session.getAttribute(Globals.TOKEN_KEY)%>';
form.submit();
}
</script>