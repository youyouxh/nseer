<!--
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 -->
<%@page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*,include.nseer_cookie.*" import="java.util.*" import="java.io.*" import ="include.nseer_db.*,java.text.*"%>
<%@include file="../../include/head.jsp"%>
<jsp:useBean id="demo" class="include.tree_index.businessComment" scope="page"/>
<%nseer_db document_db = new nseer_db((String)session.getAttribute("unit_db_name"));%>
<% DealWithString DealWithString=new DealWithString(application);
String mod=request.getRequestURI();
	 demo.setPath(request);
	 String handbook=demo.businessComment(mod,"您正在做的业务是：","document_main","reason","value");%>
<%
String status="0";
String sql = "select * from document_config_public_char where kind='调试状态'";
ResultSet rs = document_db.executeQuery(sql) ;
if(rs.next()){
if(rs.getString("type_name").equals("1")){
status="1";
}else{
status="0";
}
}
document_db.close();
%>
<script language="javascript" src="input_control/validation-framework.js"></script>
 <table <%=TABLE_STYLE2%> class="TABLE_STYLE2">
 <tr <%=TR_STYLE1%> class="TR_STYLE1">
 <td <%=TD_HANDBOOK_STYLE1%> class="TD_HANDBOOK_STYLE1"><div class="div_handbook"><%=handbook%></div></td>
 </tr>
 </table>
   <div id="nseerGround" class="nseerGround">
<form id="mutiValidation" class="x-form" method="POST" action="../../../document_config_debug_status_change_ok">
 <table <%=TABLE_STYLE2%> class="TABLE_STYLE2">
 <tr <%=TR_STYLE1%> class="TR_STYLE1">
 <td <%=TD_STYLE3%> class="TD_STYLE3"><div <%=DIV_STYLE1%> class="DIV_STYLE1"><input type="submit" <%=SUBMIT_STYLE1%> class="SUBMIT_STYLE1" value="<%=demo.getLang("erp","提交")%>">&nbsp;<input type="button" <%=BUTTON_STYLE1%> class="BUTTON_STYLE1" value="<%=demo.getLang("erp","返回")%>" onClick=location="status.jsp"></div></td>
 </tr>
 </table>
<table <%=TABLE_STYLE1%> class="TABLE_STYLE1" width="100%" id=theObjTable>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="20%"><%=demo.getLang("erp","调试状态")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="80%"><%if(status.equals("1")){%><input type="radio" <%=RADIO_STYLE1%> class="RADIO_STYLE1" name="status" value='0'><%=demo.getLang("erp","正在调试")%><input type="radio" <%=RADIO_STYLE1%> class="RADIO_STYLE1" name="status" value='1' checked><%=demo.getLang("erp","调试完成")%><%}else{%><input type="radio" name="status" value='0' checked><%=demo.getLang("erp","正在调试")%><input type="radio" <%=RADIO_STYLE1%> class="RADIO_STYLE1" name="status" value='1'><%=demo.getLang("erp","调试完成")%><%}%></td>
</tr>
</table>
</form>
</div>