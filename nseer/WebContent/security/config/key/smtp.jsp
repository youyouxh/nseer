<!--
 *this file is part of nseer erp
 *Copyright (C)2006-2010 Nseer(Beijing) Technology co.LTD/http://www.nseer.com 
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either
 *version 2 of the License, or (at your option) any later version.
 -->
<%@page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*,include.nseer_cookie.*" import="java.util.*" import="java.io.*" import="include.nseer_db.*,java.text.*"%>
<jsp:useBean id="demo" class="include.tree_index.businessComment" scope="page"/>
<% DealWithString DealWithString=new DealWithString(application);
String mod=request.getRequestURI();
	 demo.setPath(request);
	 String handbook=demo.businessComment(mod,"您正在做的业务是：","document_main","reason","value");%>
<%@include file="../../include/head.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<form id="mutiValidation" method="POST" action="../../../security_config_key_smtp_ok">
<table <%=TABLE_STYLE2%> class="TABLE_STYLE2">
 <tr <%=TR_STYLE1%> class="TR_STYLE1">
	<td <%=TD_HANDBOOK_STYLE1%> class="TD_HANDBOOK_STYLE1"><div class="div_handbook"><%=handbook%></div></td>
 </tr>
 </table>
<table <%=TABLE_STYLE6%> class="TABLE_STYLE6">
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE3%> class="TD_STYLE3"></td>
<td <%=TD_STYLE3%> class="TD_STYLE3"><div <%=DIV_STYLE1%> class="DIV_STYLE1"><input type="submit" <%=BUTTON_STYLE1%> class="BUTTON_STYLE1" value="<%=demo.getLang("erp","设置")%>" onClick=""></div>&nbsp;</td>
</TR>
</table>
<%
String[] sh=SmtpHost.getContent((String)session.getAttribute("unit_db_name"));
%>
<div id="nseerGround" class="nseerGround">
<table <%=TABLE_STYLE1%> class="TABLE_STYLE1" id=theObjTable>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="20%"><%=demo.getLang("erp","邮件服务器")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="80%"><input type="text" name="Mailhost" style="width:200px;hight:10px" value="<%=sh[0]%>"><%=demo.getLang("erp","如:smtp.sina.com")%>
</TR>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="20%"><%=demo.getLang("erp","用户名")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="80%"><input type="text" name="eName" style="width:200px;hight:10px" value="<%=sh[1]%>"><%=demo.getLang("erp","如:yhuser@sina.com")%></td>
</TR>
<tr <%=TR_STYLE1%> class="TR_STYLE1">
<td <%=TD_STYLE11%> class="TD_STYLE1" width="20%"><%=demo.getLang("erp","密码")%></td>
<td <%=TD_STYLE21%> class="TD_STYLE2" width="80%"><input type="password" name="ePwd" style="width:200px;hight:10px" value="<%=sh[2]%>"></td>
</TR>
</table>
</div>